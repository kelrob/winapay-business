<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use App\Models\User;

class BusinessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Sentinel::getUser();

        if (Sentinel::check() && User::with('subscription')->find($user->id)->subscription != null) {
            return $next($request);
        } else {
            return redirect(url('sign-out'));
        }
    }
}
