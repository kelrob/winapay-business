<?php

namespace App\Http\Controllers\Utils;

use App\Models\GamesPlayed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Charts;
use Sentinel;
use DB;

class Chart extends Controller
{
    public function index() {
        $loggedUser = Sentinel::getUser();

        $gamesPlayed = GamesPlayed::where('user_id', $loggedUser->id)->get();
        $total = GamesPlayed::where('user_id', $loggedUser->id)
            ->whereRaw('year(`created_at`) = ?', date('Y'))
            ->count();

        $chart = Charts::database($gamesPlayed, 'bar', 'highcharts')
            ->title("Monthly Plays")
            ->elementLabel("Total Game play this year: " . number_format($total))
            ->dimensions(1000, 500)
            ->responsive(true)
            ->groupByMonth(date('Y'), true);

        $pie  =	 Charts::create('pie', 'highcharts')
            ->title('Demography')
            ->labels(['Male', 'Female'])
            ->values([200,500])
            ->dimensions(1000,500)
            ->responsive(true);

        return view('Dashboard.statistics',compact('chart', 'pie', 'loggedUser'));
    }
}
