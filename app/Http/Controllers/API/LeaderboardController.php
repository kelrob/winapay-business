<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuizGames;
use App\Models\QuizResponse;
use App\Models\EssayResponse;

class LeaderboardController extends Controller
{
    public function essayLeaderboard($id) {
        //$game = QuizGames::find($id);
        $response = EssayResponse::where('essay_id', $id)->orderBy('score', 'DESC')->get();

        return [
            'status' => 'success',
            'data' => $response
        ];
    }

    public function quizLeaderboard($id) {
        //$game = QuizGames::find($id);
        $response = QuizResponse::where('game_id', $id)
            ->orderBy('right_answer', 'DESC')
            ->orderBy('time_spent', 'ASC')
            ->get();

        return [
            'status' => 'success',
            'data' => $response
        ];
    }

    public function userHasPlayedQuiz($gameId, $winUserId) {
        $played  = QuizResponse::where('game_id', $gameId)
                    ->where('win_user_id', $winUserId)
                    ->count();

        if ($played >= 1) {
            $response = true;
        } else {
            $response = false;
        }

        return [
            'status' => 'success',
            'played' => $response
        ];
    }
}
