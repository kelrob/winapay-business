<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\QuizGames;
use App\Models\Fan;
use Validator;
use Sentinel;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public $successStatus = 200;

    public function login() {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $token =  $user->createToken('BusinessWinapay')-> accessToken;
            return response()->json(
                [
                    'status' => true,
                    'token' => $token,
                    'data' => $user
                ],
                $this->successStatus
            );
        } else{
            return response()->json(
                [
                    'status' => false,
                    'error' => 'Incorrect Login Credentials'
                ], 401
            );
        }
    }


    public function register(Request $request)
    {
        # Possible error messages
        $errorMessages = [
            'business_name.required' => 'Business Name is required',
            'phone.required' => 'Phone number is required',
            'phone.unique' => 'Phone number has already been taken',
            'email.required' => 'Email is required',
            'email.email' => 'Invalid email address passed',
            'email.unique' => 'Email has already been taken',
            'password.required' => 'Password is required',
            'password_again.same' => 'Both Password does not match',
            'category.required' => 'Category is required'
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'business_name' => 'required',
                'phone' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'password_again' => 'required|same:password',
                'category' => 'required'
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Account Creation failed',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }

        # Exclude The Password Again field from going to db
        $request->offsetUnset('password_again');

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        $token =  $user->createToken('BusinessWinapay')->accessToken;
        return response()->json(
            [
                'status' => true,
                'response' => 'Account Created Successfully',
                'token' => $token,
                'data' => array(
                    'business_name' => $request->business_name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'category' => $request->category,
                ),
                'user_id' => $user->id
            ],
            $this-> successStatus
        );
    }

    public function userProfile($id) {

        $user = User::where('id', $id)->first();

        return [
            'status' => true,
            'user' => $user
        ];

    }

    public function allBrands($limit) {
        $brands = User::inRandomOrder()->limit($limit)->get();

        $data = [];
        foreach ($brands as $brand) {
            $data[] = [
                'id' => $brand->id,
                'business_name' => $brand->business_name,
                'email' => $brand->email,
                'wallet' => $brand->wallet,
                'phone' => $brand->phone,
                'website' => $brand->website,
                'address' => $brand->address,
                'brand_color' => $brand->brand_color,
                'facebook_page' => $brand->facebook_page,
                'twitter_page' => $brand->twitter_page,
                'instagram_page' => $brand->instagram_page,
                'whatsapp_no' => $brand->whatsapp_no,
                'category' => $brand->category,
                'country' => $brand->country,
                'logo' => 'https://business.winapay.com/img/logos/' . $brand->logo,
                'first_time' => $brand->first_time,
                'last_login' => $brand->last_login,
                'created_at' => $brand->created_at,
                'updated_at' => $brand->updated_at
            ];
        }

        return [
            'status' => true,
            'brands' => $data
        ];
    }

    public function increaseWallet(Request $request) {
        $user = User::where('id', $request->brand_id)->first();
        $user->wallet = $user->wallet + $request->amount;
        $user->save();

        return [
            'status' => true,
            'user' => $user
        ];
    }

    public function newFan(Request $request) {
        $fan = new Fan();

        $userId = $request->user_id;
        $winUserId = $request->win_user_id;

        $fanExist = Fan::where('user_id', $userId)
            ->where('win_user_id', $winUserId)
            ->count();

        if ($fanExist == 0) {
            $fan->user_id = $request->user_id;
            $fan->win_user_id = $request->win_user_id;
            $fan->save();

            return true;
        }
    }
}
