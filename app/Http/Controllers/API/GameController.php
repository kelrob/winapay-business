<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuizGames;
use App\Models\User;
use App\Models\EssayResponse;
use App\Models\QuizResponse;
use App\Models\GamesPlayed;

class GameController extends Controller
{
    public function allBrandGames($limit) {

        $games = QuizGames::with('user')
            ->groupBy(['user_id'])
            ->limit($limit)
            ->get();

        return [
            "status" => true,
            "brandGame" => $games
        ];
    }

    public function brandGames($brand_id, Request $request) {
        $brandGames = QuizGames::where('user_id', $brand_id)
            ->orderBy('id', 'DESC')
            ->get();

        $data = [];
        foreach ($brandGames as $game) {
            $data[] = [
                'id' => $game->id,
                'user' => $game->user_id,
                'quiz_title' => $game->quiz_title,
                'essay_title' => $game->essay_title,
                'essay_description' => $game->essay_description,
                'character_no' => $game->character_no,
                'gave_permalink' => $game->game_permalink,
                'cover_photo' => $game->cover_photo,
                'game_type' => $game->game_type,
                'post_writeup' => $game->post_writeup,
                'embedded_url' => $game->embedded_url,
                'video_desc' => $game->video_desc,
                'question' => json_decode($game->question),
                'option_a' => json_decode($game->option_a),
                'option_b' => json_decode($game->option_b),
                'option_c' => json_decode($game->option_c),
                'option_d' => json_decode($game->option_d),
                'answer' => json_decode($game->answer),
                'time' => $game->time,
                'fans_action' => $game->fans_action,
                'download_link' => $game->download_link,
                'website_url' => $game->website_url,
                'twitter' => $game->twitter,
                'instagram' => $game->instagram,
                'reward_type' => $game->reward_type,
                'count_reward_winners' => $game->count_reward_winners,
                'reward_amount' => $game->reward_amount,
                'gift_people_count' => $game->gift_people_count,
                'gift_product_name' => $game->gift_product_name,
                'ticket_type' => $game->ticket_type,
                'ticket_people_count' => $game->ticket_people_count,
                'game_category' => $game->game_category,
                'paid_game_amount' => $game->paid_game_amount,
                'tracking_key' => $game->tracking_key,
                'month_year' => $game->month_year,
                'game_closed' => $game->game_closed,
                'created_at' => $game->created_at,
                'updated_at' => $game->updated_at,
                'brand_id_info'  => User::find($game->user_id)
            ];
        }
        if ($request->exists('data')) {
            return response()->json(
                [
                    'status' => true,
                    'brandGames' => $data,
                ], 200
            );
        } else {
            return [
                "status" => true,
                "brandGames" => $brandGames
            ];
        }
    }

    public function fetchGame($game_id) {
        $game = QuizGames::with('user.brand_subscription_mode')
            ->where('id', $game_id)
            ->first();


        return [
            "status" => true,
            "game" => $game
        ];
    }

    public function newEssayResponse(Request $request) {
        $input = $request->all();
        EssayResponse::create($input);

        return [
            "status" => true,
            "data" => $input
        ];
    }

    public function newQuizResponse(Request $request) {

        $request['time_spent'] = $request->end_time - $request->start_time;
        $input = $request->all();

        QuizResponse::create($input);

        return [
            "status" => true,
            "data" => $input
        ];
    }

    public function discardQuizResponse(Request $request) {
        $gameId = $request->game_id;
        $userId = $request->user_id;

        QuizResponse::where('game_id', $gameId)
            ->where('win_user_id', $userId)
            ->delete();

        return [
            "status" => true,
        ];
    }

    public function newGamePlayed(Request $request) {
        $gamePlay = new GamesPlayed();

        $gamePlay->user_id = $request->user_id;
        $gamePlay->win_user_id = $request->win_user_id;
        $gamePlay->game_id = $request->game_id;
        $gamePlay->game_type = $request->game_type;
        $gamePlay->save();
    }

    public function tryAgainExpire ($userId, $gameId) {
        $triedAgain = QuizResponse::where('win_user_id', $userId)
            ->where('game_id', $gameId)
            ->where('try_again', 1)
            ->count();

        if ($triedAgain >= 1) {
            $response = true;
        } else {
            $response = false;
        }

        return [
            'status' => 'success',
            'tried_again' => $response
        ];
    }

}
