<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Sentinel;
use App\Models\QuizGames;
use App\Models\EssayResponse;
use App\Models\User;

class Dashboard extends Controller
{
    public function index() {
        # Get the logged in User Id
        $user = Sentinel::getUser();
        $loggedUser = User::with('subscription')->find($user->id);

        # Logged In User Games
        $userGames = QuizGames::where('user_id', $loggedUser->id)->orderBy('id', 'desc')->get();

        # User Total games
        $totGames = QuizGames::where('user_id', $loggedUser->id)->get()->count();

        return view('Dashboard.index', compact('userGames', 'totGames', 'loggedUser'));
    }

    public function essayResponse() {
        # Get the logged in User Id
        $loggedUser = Sentinel::getUser();

        $essayGames = QuizGames::where('user_id', $loggedUser->id)
                    ->where('quiz_title', '-')
                    ->get();

        return view('Dashboard.essayResponse', compact('loggedUser', 'essayGames'));
    }

    public function essayResponseInfo($game_id) {
        $loggedUser = Sentinel::getUser();

        $essay_title = QuizGames::find($game_id)->essay_title;
        $responses = EssayResponse::where('essay_id', $game_id)->get();

        return view('Dashboard.response_info', compact('loggedUser', 'responses', 'essay_title'));
    }

    public function responseDetails($response_id) {
        $loggedUser = Sentinel::getUser();

        $response = EssayResponse::where('id', $response_id)->first();
        return view('Dashboard.response_details', compact('loggedUser', 'response'));
    }

    public function statistics() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.statistics', compact('loggedUser'));
    }

    public function createCampaign() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.create_campaign', compact('loggedUser'));
    }

    public function createQuizCampaign() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.create_quiz_campaign', compact('loggedUser'));
    }

    public function createEssayCampaign() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.create_essay_campaign', compact('loggedUser'));
    }

    public function viewQuizGame($game_id, $game_permalink) {
        $loggedUser = Sentinel::getUser();
        $userGame = QuizGames::find($game_id);

        if ($loggedUser->id == $userGame->user_id) {
            $data = [
                'default_count' => 0,
                'game_question' => json_decode($userGame->question),
                'option_a' => json_decode($userGame->option_a),
                'option_b' => json_decode($userGame->option_b),
                'option_c' => json_decode($userGame->option_c),
                'option_d' => json_decode($userGame->option_d),
                'answer' =>json_decode($userGame->answer),
                'question_count' => count(json_decode($userGame->question))
            ];

            return view('Dashboard.view_quiz_game', compact('userGame', 'data', 'loggedUser'));
        } else {
            return redirect( url('404'));
        }
    }

    public function viewEssayGame($game_id, $game_permalink) {
        $loggedUser = Sentinel::getUser();
        $userGame = QuizGames::find($game_id);
        //dd($userGame);
        if ($loggedUser->id == $userGame->user_id) {

            $data = [
                'default_count' => 0,
            ];

            return view('Dashboard.view_essay_game', compact('userGame', 'data', 'loggedUser'));

        } else {
            return redirect( url('404'));
        }
    }

    public function editQuizGame($game_id, $game_permalink) {
        $loggedUser = Sentinel::getUser();
        $userGame = QuizGames::find($game_id);

        if ($loggedUser->id == $userGame->user_id) {
            $data = [
                'default_count' => 0,
                'game_question' => json_decode($userGame->question),
                'option_a' => json_decode($userGame->option_a),
                'option_b' => json_decode($userGame->option_b),
                'option_c' => json_decode($userGame->option_c),
                'option_d' => json_decode($userGame->option_d),
                'answer' =>json_decode($userGame->answer),
                'question_count' => count(json_decode($userGame->question))
            ];
            return view('Dashboard.edit_quiz_game', compact('userGame', 'data', 'loggedUser'));
        } else {
            return redirect( url('404'));
        }
    }

    public function editEssayGame($game_id, $game_permalink) {
        $loggedUser = Sentinel::getUser();
        $userGame = QuizGames::find($game_id);

        if ($loggedUser->id == $userGame->user_id) {

            return view('Dashboard.edit_essay_game', compact('userGame',  'loggedUser'));
        } else {
            return redirect( url('404'));
        }
    }

    public function fundWallet() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.fund_wallet', compact('loggedUser'));
    }

    public function howItWorks() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.how_it_works', compact('loggedUser'));
    }

    public function contactUs() {
        $loggedUser = Sentinel::getUser();
        return view('Dashboard.contact_us', compact('loggedUser'));
    }

    public function mail()
    {
        $name = 'Krunal';
        Mail::to('robertebafua@gmail.com')->send(new SendMail($name));

        return 'Email was sent';
    }
}
