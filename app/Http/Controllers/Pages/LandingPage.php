<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\User;

class LandingPage extends Controller
{
    public function index() {
        return view('landing-page.index');
    }

    public function firstLogin() {
        # Get the logged in User Info
        $user = Sentinel::getUser();

        # Update first user first time as false
        $user->first_time = 0;
        $user->save();

        return view('landing-page.first_login');
    }

    public function selectPackage() {
        # Get the logged in User Info
        $user = Sentinel::getUser();
        $profile = User::with('subscription')->find($user->id);

        if ($profile->subscription == null) {
            return view('landing-page.select-plan', compact('profile'));
        } else {
            return redirect(url('/dashboard'));
        }

    }
}
