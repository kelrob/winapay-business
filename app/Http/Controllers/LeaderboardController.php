<?php

namespace App\Http\Controllers;

use App\Models\EssayResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\QuizGames;
use App\Models\QuizResponse;

class LeaderboardController extends Controller
{
    public function quizLeaderboard($id) {
        $loggedUser = Sentinel::getUser();

        $game = QuizGames::find($id);
        $response = QuizResponse::where('game_id', $id)
            ->orderBy('right_answer', 'DESC')
            ->orderBy('time_spent', 'ASC')
            ->get();

        return view('Dashboard.quiz_leaderboard', compact('loggedUser', 'game', 'response'));
    }

    public function essayLeaderboard($id) {
        $loggedUser = Sentinel::getUser();

        $game = QuizGames::find($id);
        $response = EssayResponse::where('essay_id', $id)
            ->orderBy('score', 'DESC')
            ->get();

        return view('Dashboard.essay_leaderboard', compact('loggedUser', 'game', 'response'));
    }

    public function rewardUser($id) {

        $loggedUser = Sentinel::getUser();
        $brand = $loggedUser->business_name;

        # Find The Game
        $game = QuizGames::find($id);

        if ($game->game_ended != 1) {
            # Check if Game is Quiz or Essay
            if ($game->essay_title == null) {
                $gameType = 'quiz';
            } else {
                $gameType = 'essay';
            }

            # Get the no of people to reward by brand
            if ($game->reward_type == 'cash'){
                $rewardAmount = $game->reward_amount;
                $amountToGive = $rewardAmount - (0.03 * $rewardAmount);
                $winnersCount =  $game->count_reward_winners;
            } else if ($game->reward_type == 'gift') {
                $winnersCount =  $game->gift_people_count;
            } else if ($game->reward_type == 'ticket') {
                $winnersCount =  $game->ticket_people_count;
            }


            # Get the total count of response on game
            if ($gameType == 'quiz') {

                $responseCount = QuizResponse::where('game_id', $id)->count();

                $response = QuizResponse::where('game_id', $id)
                    ->orderBy('right_answer', 'DESC')
                    ->orderBy('time_spent', 'ASC')
                    ->limit($winnersCount)
                    ->get();

            } else if ($gameType == 'essay') {

                $responseCount = EssayResponse::where('essay_id', $id)->count();

                $response = EssayResponse::where('essay_id', $id)
                    ->orderBy('score', 'DESC')
                    ->limit($winnersCount)
                    ->get();
            }

            $remainder = $winnersCount - $responseCount;

            # Check if Response >= total no of winners selected by brand
            if ($responseCount >= $winnersCount) {

                # Disburse if Reward is Cash
                if ($game->reward_type == 'cash') {

                    foreach ($response as $key) {

                        // Send SMS
                        $winnerFullname = ucwords(userData($key->win_user_id)->fullname);
                        $winnerPhone = userData($key->win_user_id)->phone;

                        increaseWinUserWallet($key->win_user_id, $amountToGive);
                        $key->winner = 1;
                        $key->save();

                        $sms = 'Hello  ' . $winnerFullname . ', you have been selected among winners. Your wallet has been credited with the cash price by ' . $brand;

                        sendSms($winnerPhone, $sms);
                    }

                    $game->game_ended = 1;
                    $game->save();

                    $message = 'Winners Rewarded Respectfully with <b>&#8358;' . $rewardAmount . '</b> in their wallet. <br /> Game has now ended';
                }

                if ($game->reward_type == 'gift') {

                    foreach ($response as $key) {
                        // Send SMS
                        $winnerFullname = ucwords(userData($key->win_user_id)->fullname);
                        $winnerPhone = userData($key->win_user_id)->phone;

                        $key->winner = 1;
                        $key->save();

                        $sms = 'Hello  ' . $winnerFullname . ', you have been selected among winners. Please contact ' . $brand . ' to get your gift.';

                        sendSms($winnerPhone, $sms);
                    }

                    $game->game_ended = 1;
                    $game->save();

                    $message = $winnersCount . ' Winners have been contacted respectfully. Kindly do well to reward selected winners on your dashboard. <br /> This Game is now ended.';

                }

                if ($game->reward_type == 'ticket') {

                    foreach ($response as $key) {
                        // Send SMS
                        $winnerFullname = ucwords(userData($key->win_user_id)->fullname);
                        $winnerPhone = userData($key->win_user_id)->phone;

                        $key->winner = 1;
                        $key->save();

                        $sms = 'Hello  ' . $winnerFullname . ', you have been selected among winners. Please contact ' . $brand . ' to get your ticket.';
                        sendSms($winnerPhone, $sms);
                    }

                    $game->game_ended = 1;
                    $game->save();

                    $message = $winnersCount . ' Winners have been contacted respectfully. Kindly do well to reward selected winners on your dashboard. <br /> This Game is now ended.';
                }

            } else {
                $message = '<b>' . $responseCount . ' people </b> has played this game, remaining <b>' . $remainder . ' more people</b> before you can reward winners.';
            }

            return [
                'status' => true,
                'data' => $message,
                'response' => 'success'
            ];
        } else {
            $message = '<b class="text-danger"><i class="fa fa-info-circle fa-2x"></i></b> Fans have already been rewarded for this game and this game has also ended';
            return [
                'status' => false,
                'data' => $message,
                'response' => 'success'
            ];
        }
    }
}
