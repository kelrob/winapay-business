<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paystack;
use Sentinel;
use App\Models\User;
use App\Models\WithdrawalRequest;

class PaymentController extends Controller
{
    /**
    * Redirect the User to Paystack Payment Page
    * @return Url
    */
    public function redirectToGateway()
    {
        // Convert to Kobo
        $charges = (request()->amount / 100) * (1.5);
        request()->amount = (request()->amount + $charges) * 100;

        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {

    }

    public function increaseWallet($email, $amountKobo) {

        $user = User::where('email', $email)->first();

        $amount = $amountKobo / 100;
        $charges = ($amount / 100) * (1.5);

        $user->wallet = $user->wallet + ($amount - $charges);
        $user->save();

        return true;
    }

    public function withdrawalRequest() {

        $loggedUser = Sentinel::getUser();
        $amount = request()->amount;

        if (empty($amount)) {
            return [
                'status' => false,
                'data' => 'Amount Can not be empty'
            ];
        }

        if ($amount < 30000) {
            return [
                'status' => false,
                'data' => 'Amount Can not be less than &#8358;30,000'
            ];
        }

        if ($loggedUser->wallet > $amount) {

            # Process withdrawal Request
            $withdrawalRequest = new WithdrawalRequest();

            $countRequest = $withdrawalRequest::where('user_id', $loggedUser->id)->where('status', 0)->count();

            if ($countRequest == 0) {
                $withdrawalRequest->user_id = $loggedUser->id;
                $withdrawalRequest->amount = $amount;
                $withdrawalRequest->status = 0;
                $withdrawalRequest->save();

                return [
                    'status' => true,
                    'data' => 'Withdrawal request of <b>&#8358;'. $amount .'</b> has been placed successfully</b>'
                ];
            } else {
                return [
                    'status' => false,
                    'data' => 'You already have a pending withdrawal request.</b>'
                ];
            }

        } else {
            return [
                'status' => false,
                'data' => 'Insufficient Amount in Wallet'
            ];
        }
    }

    public function bankTransfer(Request $request) {
        $loggedUser = Sentinel::getUser();

        $brandEmail = $loggedUser->email;

        $depositorName = request()->depositor_name;
        $amountPaid = request()->amount_paid;
        $paymentDate = request()->payment_date;

        $amountPaid = number_format($amountPaid);

        if (empty($depositorName) || empty($amountPaid) || empty($paymentDate)) {
            return [
                'status' => false,
                'data' => 'All fields are required'
            ];
        }

        $message = "$brandEmail, $depositorName paid $amountPaid on $paymentDate - Winapay business ";
        sendSms('08066781458', $message);

        return [
            'status' => true,
            'data' => 'Submitted Successfully. Your account will be credited shortly.'
        ];
    }
}
