<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\Models\BrandSubscriptionMode;

class LoginController extends Controller
{
    public function login() {
        return view('security.login');
    }

    public function register() {
        return view('security.register');
    }

    public function forgotPassword() {
        return view('security.forgot-password');
    }

    public function registerBusiness(Request $request) {

        # All fields Input
        $info = $request->all();

        # Custom Error Messages
        $errorMessages = [
            'business_name.required' => 'This field is Required',
            'email.required' => 'This field is Required',
            'email.email' => 'This must be a valid email address',
            'email.unique' => 'This email address has already been taken',
            'category.required' => 'This field is Required',
        ];

        # Validation
        $validateData = Validator::make($request->all(),
            [
                'business_name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => 'required|unique:users',
                'category' => 'required',
                'password' => 'required|min:6',
                'password_again' => 'required|min:6',
            ], $errorMessages
        );

        # If Validation fails
        if ($validateData->fails()) {
            return Redirect::back()->withInput()->withErrors($validateData->messages());
        }

        if ($request->input('password') == $request->input('password_again')) {
            $user = Sentinel::registerAndActivate($info);

            BrandSubscriptionMode::create([
                'user_id' => $user->id,
                'status' => 0,
            ]);

            return view('security.create-success');
        } else {
            return Redirect::back()->withInput()->withErrors(['Password and Password again does not match', 'The Message']);
        }

    }

    public function loginBusiness(Request $request) {
        Sentinel::disableCheckpoints();

        $errorMessages = [
            'email.required' => 'This Field is required',
            'email.email' => 'Invalid Email Address',
            'password.required' => 'This Field is required'
        ];

        $validateData = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ], $errorMessages);

        # If Validation fails
        if ($validateData->fails()) {
            return Redirect::back()->withInput()->withErrors($validateData->messages());
        }

        try {
            $user = Sentinel::authenticate($request->all());
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $returnData = array(
                'status' => 'error',
                'message' => 'Please review',
                'errors' => ["You are banned for $delay seconds"]
            );
            return Redirect::back()->withInput()->withErrors(["You are banned for $delay seconds", 'The Message']);
        } catch (NotActivatedException $e) {
            $returnData = array(
                'status' => 'error',
                'message' => 'Please review',
                'errors' => ["Go and activate your account"]
            );
            return Redirect::back()->withInput()->withErrors(["Go and activate your account", 'The Message']);
        }

        if (Sentinel::check() != false) {
            if (Sentinel::getUser()->first_time == 1) {
                return redirect(url('/select-plan'));
            } else {
                return redirect(url('/dashboard'));
            }

        } else {
            return Redirect::back()->withInput()->withErrors(["Invalid Login Credentials", 'The Message']);
        }
    }

    public function logout() {
        Sentinel::logout();
        return redirect(url('/sign-in'));
    }
}
