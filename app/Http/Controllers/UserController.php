<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\File;
use App\Models\User;
use App\Models\BrandSubscriptionMode;
use App\Mail\RewardMail;

class UserController extends Controller
{
    public function index() {

        # Get the logged in User Id
        $user = Sentinel::getUser();

        $loggedUser = User::with('subscription', 'brand_subscription_mode')->find($user->id);
        return view('Dashboard.profile', compact('loggedUser'));
    }

    public function updateProfile(Request $request) {
        # Get the logged in User Id for use
        $loggedUser = Sentinel::getUser();

        //$loggedUser->update($request->all());
        $loggedUser->website = $request->website;
        $loggedUser->address = $request->address;
        $loggedUser->phone = $request->phone;
        $loggedUser->brand_color = $request->brand_color;
        $loggedUser->facebook_page = $request->facebook_page;
        $loggedUser->twitter_page = $request->twitter_page;
        $loggedUser->instagram_page = $request->instagram_page;
        $loggedUser->whatsapp_no = $request->whatsapp_no;

        if ($loggedUser->save()) {
            return [
                'status' => true,
                'user' => $loggedUser
            ];
        } else {
            return [
                'status' => false,
                'user' => $loggedUser
            ];
        }

    }

    public function changeLogo(Request $request) {
        # Get the logged in User Id for use
        $loggedUser = Sentinel::getUser();

        # Start processing image upload
        $logo = $request->file('logo');
        $dir =  $_SERVER['DOCUMENT_ROOT'];
        $original_directory = "$dir/img/logos";
        $month_year = strtolower(date('F-Y'));
        $target_directory = $original_directory . '/' . $month_year;

        # Check if directory exist or make one
        if (!is_dir($target_directory)) {
            File::makeDirectory($target_directory, $mode = 0777, true, true);
        }

        //dd($logo);
        $filename = rand(1000,1000000) . $logo->getClientOriginalName();
        $dbLogoName = $month_year . '/' .$filename;

        $uploadFile = $logo->move($target_directory, $filename);
        if ($uploadFile) {
            $loggedUser->logo = $dbLogoName;

            if ($loggedUser->save()) {
                return [
                    'status' => true,
                    'user' => $loggedUser
                ];
            } else {
                return [
                    'status' => false,
                    'user' => $loggedUser
                ];
            }
        }
    }

    public function subscriptionOn (Request $request) {
        # Get the logged in User Id for use
        $loggedUser = Sentinel::getUser();

        $brandSubscription = BrandSubscriptionMode::where('user_id', $loggedUser->id)->first();
        $brandSubscription->status = 1;
        $brandSubscription->amount = $request->amount;

        if ($brandSubscription->save()) {
            return [
                'status' => true,
                'data' => $brandSubscription
            ];
        } else {
            return [
                'status' => false,
                'data' => $brandSubscription
            ];
        }
    }

    public function subscriptionOff (Request $request) {
        # Get the logged in User Id for use
        $loggedUser = Sentinel::getUser();

        $brandSubscription = BrandSubscriptionMode::where('user_id', $loggedUser->id)->first();
        $brandSubscription->status = 0;
        $brandSubscription->amount = null;

        if ($brandSubscription->save()) {
            return [
                'status' => true,
                'data' => $brandSubscription
            ];
        } else {
            return [
                'status' => false,
                'data' => $brandSubscription
            ];
        }
    }

    public function testEmail () {
        $data = [
            'name' => 'Robert Ebafua',
            'email'=> 'robertebafua@gmail.com'
        ];

        \Mail::to('robertebafua@gmail.com')->send(new RewardMail($data));

        return 'Mail Sent';
    }
}
