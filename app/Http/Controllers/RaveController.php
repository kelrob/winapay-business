<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Rave;
use App\Models\User;
use App\Models\Subscription;

class RaveController extends Controller
{
    /**
     * Initialize Rave payment process
     * @return void
     */
    public function initialize()
    {
        //This initializes payment and redirects to the payment gateway
        //The initialize method takes the parameter of the redirect URL
        Rave::initialize(url('/rave/callback'));
    }

    /**
     * Obtain Rave callback information
     *
     */
    public function callback()
    {

        //4
        $response = json_decode(request()->resp, true);
        $txRef = $response['tx']['txRef'];

        $data = Rave::verifyTransaction($txRef);

        if ($data->status == 'success') {
            $businessEmail = $data->data->custemail;
            $amount = $data->data->amount;

            if ($amount == 2000) {
                $alias = 'starter';
                $plan = 'Starter';
                $days_left = 30;
            } else if ($amount == 10000) {
                $alias = 'business';
                $plan = 'Business';
                $days_left = 180;
            } else if ($amount == 18000) {
                $alias = 'one_time';
                $plan = 'One Time';
                $days_left = 365;
            }

            if ($amount == 2000 || $amount == 10000 || $amount == 18000) {
                $user = User::where('email', $businessEmail)->first();

                $subscription = new Subscription();
                $subscription->user_id = $user->id;
                $subscription->plan = $plan;
                $subscription->alias = $alias;
                $subscription->days_left = $days_left;
                $subscription->save();

                $user->first_time = 0;
                $user->save();

                return redirect(url('/dashboard'));
            } else {
                return 'Payment Error';
            }

        }

        // Get the transaction from your DB using the transaction reference (txref)
        // Check if you have previously given value for the transaction. If you have, redirect to your successpage else, continue
        // Comfirm that the transaction is successful
        // Confirm that the chargecode is 00 or 0
        // Confirm that the currency on your db transaction is equal to the returned currency
        // Confirm that the db transaction amount is equal to the returned amount
        // Update the db transaction record (includeing parameters that didn't exist before the transaction is completed. for audit purpose)
        // Give value for the transaction
        // Update the transaction to note that you have given value for the transaction
        // You can also redirect to your success page from here

    }
}
