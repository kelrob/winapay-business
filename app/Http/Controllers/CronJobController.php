<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class CronJobController extends Controller
{
    public function countdownPlanSub() {
        DB::table('subscription')->decrement('days_left');
    }

    public function deletePlanSub() {
        $expiring = Subscription::where('days_left', '<', 1)->get();

        foreach ($expiring as $expired) {
            $user = User::find($expired->user_id);
            $user->first_time = 1;
            $user->save();

            $expired->delete();
        }
    }

    public function autoRenewPlanSub() {
        $expirySoon = Subscription::where('days_left', 0)->get();

        foreach ($expirySoon as $expired) {
            $user = User::with('subscription')->find($expired->user_id);
            $currentSub = $user->subscription->alias;

            if ($currentSub == 'starter') {
                $amountToDeduct = 5 * 360;
            } else if ($currentSub == 'business') {
                $amountToDeduct = 7 * 360;
            } else if ($currentSub == 'one_time') {
                $amountToDeduct = 37 * 360;
            }

            if ($user->wallet >= $amountToDeduct) {

                # Deduct Money from user wallet
                $user->wallet = $user->wallet - $amountToDeduct;
                $user->save();

                # Delete Old Sub Record
                Subscription::where('user_id', $expired->user->id)->delete();

                # Subscribe User
                $subscription = new Subscription();
                $subscription->user_id = $expired->user->id;
                $subscription->plan = $user->subscription->plan;
                $subscription->alias = $user->subscription->alias;
                $subscription->days_left = 30;
                $subscription->save();
            }
        }
    }
}
