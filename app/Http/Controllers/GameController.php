<?php

namespace App\Http\Controllers;

use App\Models\EssayResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Validator;
use App\Models\QuizGames;
use Sentinel;

class GameController extends Controller
{
    public function createEssayCampaign(Request $request) {
        # Get the logged in User Id
        $loggedUser = Sentinel::getUser();

        # Possible error messages
        $errorMessages = [
            'cover_image.mimes' => 'Only Jpeg, Jpg and Png Images are allowed',
            'cover_image.required' => 'Image can not be empty',
            'essay_title.required' => 'Essay Title is required',
            'reward_type.required' => 'Reward type is required',
            'essay_description.required' => 'Essay Description is required',
            'character_no' => 'Character Number is required',
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'cover_image' => 'mimes:jpeg,jpg,png|required',
                'reward_type' => 'required',
                'character_no' => 'required',
                'essay_description' => 'required'
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return [
                'res' => 'failed',
                'message' => 'Game Creation failed',
                'errors' => $validator->errors()->all()
            ];
        }

        if ($request->game_category == 'paid') {
            if ($request->paid_game_amount > $loggedUser->wallet) {
                return [
                    'status' => 'failed2',
                    'message' => 'Insufficient Amount in wallet to create a paid Game'
                ];
            }

        }

        if ($request->fans_action == 'none') {
            return [
                'status' => 'failed2',
                'message' => 'Please Select Select what you want your fans to do [page 3]'
            ];
        }

        if ($request->fans_action == 'download') {
            if ($request->download_link == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Download Link cannot be empty [page 3]'
                ];
            }
        }

        if ($request->fans_action == 'website_visit') {
            if ($request->website_url == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Website URL cannot be empty [page 3]'
                ];
            }
        }

        if ($request->fans_action == 'social_media') {
            if ($request->twitter == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Twitter URL cannot be empty [page 3]'
                ];
            }

            if ($request->facebook == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Facebook URL cannot be empty [page 3]'
                ];
            }

            if ($request->instagram == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Instagram URL cannot be empty [page 3]'
                ];
            }
        }

        if ($request->reward_type == 'cash') {
            if ($request->count_reward_winners == null || $request->reward_amount == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to reward cash and and Cash amount can not be empty [page 3]'
                ];
            }
        }

        if ($request->reward_type == 'gift_product') {
            if ($request->gift_people_count == null || $request->gift_product_name == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to give Gift and and Gift Product can not be empty'
                ];
            }
        }

        if ($request->reward_type == 'ticket') {
            if ($request->ticket_people_count == null || $request->ticket_type == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to give Ticket and and Ticket Type can not be empty'
                ];
            }
        }

        if ($request->game_package == 'paid') {
            if ($request->paid_game_amount == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Please Specify Paid Game Amount'
                ];
            }
        }

        # Start processing image upload
        $coverImage = $request->file('cover_image');
        $dir =  $_SERVER['DOCUMENT_ROOT'];
        $original_directory = "$dir/img/campaigns";
        $month_year = strtolower(date('F-Y'));
        $target_directory = $original_directory . '/' . $month_year;

        # Check if directory exist or make one
        if (!is_dir($target_directory)) {
            File::makeDirectory($target_directory, $mode = 0777, true, true);
        }


        $filename = rand(1000,1000000) . $coverImage->getClientOriginalName();
        $dbCoverImageName = $month_year . '/' .$filename;

        # First image Resize
        $resizedDirectory = $target_directory . '/210x118';
        if (!is_dir($resizedDirectory)) {
            File::makeDirectory($resizedDirectory, $mode = 0777, true, true);
        }

        $image_resize = Image::make($coverImage->getRealPath());
        $image_resize->resize(210, 118);
        $image_resize->save($resizedDirectory . '/' . $filename);

        # Upload Original Image
        $uploadFile = $coverImage->move($target_directory, $filename);
        if ($uploadFile) {
            $trackingKey = randomString(6);

            # Deduct Money from the User wallet if it is a paid Game
            if ($request->game_category == 'paid') {
                $loggedUser->wallet = $loggedUser->wallet - $request->paid_game_amount;
                $loggedUser->save();
            }

            if ($request->reward_type == 'cash') {
                $amountToDeduct = $request->reward_amount * $request->count_reward_winners;

                if ($loggedUser->wallet >= $amountToDeduct) {
                    $loggedUser->wallet = $loggedUser->wallet - $amountToDeduct;
                    $loggedUser->save();
                } else{
                    return [
                        'status' => 'failed2',
                        'message' => 'Insufficient amount in wallet to reward users'
                    ];
                }
            }

            $request['user_id'] = $loggedUser->id;
            $request['cover_photo'] = $dbCoverImageName;
            $request['quiz_title'] = '-';
            $request['month_year'] = $month_year;
            $request['essay_description'] = nl2br($request->essay_description);
            $request['game_permalink'] = strtolower(createPermalink($request->essay_title));
            $request['tracking_key'] = $trackingKey;

            $request->except(['_token', 'cover_image']);

            $input = $request->all();
            QuizGames::create($input);

            return [
                'status' => 'success',
                'message' => 'Game created Successfully',
                'tracking_key' => $trackingKey
            ];

        } else {
            return [
                'status' => 'failed2',
                'message' => 'Unable to upload image at this time',
            ];
        }
    }

    public function createQuizCampaign(Request $request) {

        # Get the logged in User Id
        $loggedUser = Sentinel::getUser();

        # Possible error messages
        $errorMessages = [
            'cover_image.mimes' => 'Only Jpeg, Jpg and Png Images are allowed',
            'cover_image.required' => 'Image can not be empty',
            'game_category.required' => 'Game category can not be empty',
            'quiz_title.required' => 'Quiz Title is required',
            'game_type.required' => 'Game Type is required [page 1]',
            'time.required' => 'Timer is required',
            'reward_type.required' => 'Reward type is required',
            'question.required' => 'Question is required',
            'option_a.required' => 'Option A is required',
            'option_b.required' => 'Option B is required',
            'option_c.required' => 'Option C is required',
            'option_d.required' => 'Option D is required',
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'cover_image' => 'mimes:jpeg,jpg,png|required',
                'quiz_title' => 'required',
                'option_a' => 'required',
                'option_b' => 'required',
                'option_c' => 'required',
                'option_d' => 'required',
                'game_type' => 'required',
                'time' => 'required',
                'reward_type' => 'required',
                'question' => 'required',
                'game_category' => 'required',
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return [
                'res' => 'failed',
                'message' => 'Game Creation failed',
                'errors' => $validator->errors()->all()
            ];
        }

        if ($request->game_category == 'paid') {
            if ($request->paid_game_amount > $loggedUser->wallet) {
                return [
                    'status' => 'failed2',
                    'message' => 'Insufficient Amount in wallet to create a paid Game'
                ];
            }

        }

        if ($request->game_type == 'post' && $request->post_writeup == null) {
            return [
                'status' => 'failed2',
                'message' => 'Post Write up can not be empty [page 1]'
            ];
        }

        if ($request->game_type == 'video') {
            if ($request->embedded_url == null || $request->video_desc == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Embedded Url and Video Description can not be empty'
                ];
            }
        }

        if ($request->fans_action == 'none') {
            return [
                'status' => 'failed2',
                'message' => 'Please Select Select what you want your fans to do [page 3]'
            ];
        }

        if ($request->fans_action == 'download') {
            if ($request->download_link == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Download Link cannot be empty [page 3]'
                ];
            }
        }

        if ($request->fans_action == 'website-visit') {
            if ($request->website_url == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Website URL cannot be empty [page 3]'
                ];
            }
        }

        if ($request->fans_action == 'social_media') {
            if ($request->twitter == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Twitter URL cannot be empty [page 3]'
                ];
            }

            if ($request->facebook == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Facebook URL cannot be empty [page 3]'
                ];
            }

            if ($request->instagram == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Instagram URL cannot be empty [page 3]'
                ];
            }
        }

        if ($request->reward_type == 'cash') {
            if ($request->count_reward_winners == null || $request->reward_amount == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to reward cash and and Cash amount can not be empty [page 3]'
                ];
            }
        }

        if ($request->reward_type == 'gift_product') {
            if ($request->gift_people_count == null || $request->gift_product_name == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to give Gift and and Gift Product can not be empty'
                ];
            }
        }

        if ($request->reward_type == 'ticket') {
            if ($request->ticket_people_count == null || $request->ticket_type == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'No of people to give Ticket and and Ticket Type can not be empty'
                ];
            }
        }

        if ($request->game_package == 'paid') {
            if ($request->paid_game_amount == null) {
                return [
                    'status' => 'failed2',
                    'message' => 'Please Specify Paid Game Amount'
                ];
            }
        }

        # Start processing image upload
        $coverImage = $request->file('cover_image');
        $dir =  $_SERVER['DOCUMENT_ROOT'];
        $original_directory = "$dir/img/campaigns";
        $month_year = strtolower(date('F-Y'));
        $target_directory = $original_directory . '/' . $month_year;

        # Check if directory exist or make one
        if (!is_dir($target_directory)) {
            File::makeDirectory($target_directory, $mode = 0777, true, true);
        }


        $filename = rand(1000,1000000) . $coverImage->getClientOriginalName();
        $dbCoverImageName = $month_year . '/' .$filename;

        # First image Resize
        $resizedDirectory = $target_directory . '/210x118';
        if (!is_dir($resizedDirectory)) {
            File::makeDirectory($resizedDirectory, $mode = 0777, true, true);
        }

        $image_resize = Image::make($coverImage->getRealPath());
        $image_resize->resize(210, 118);
        $image_resize->save($resizedDirectory . '/' . $filename);

        # Upload Original Image
        $uploadFile = $coverImage->move($target_directory, $filename);
        if ($uploadFile) {
            $trackingKey = randomString(6);

            # Deduct Money from the User wallet if it is a paid Game
            if ($request->game_category == 'paid') {
                $loggedUser->wallet = $loggedUser->wallet - $request->paid_game_amount;
                $loggedUser->save();
            }

            if ($request->reward_type == 'cash') {
                $amountToDeduct = $request->reward_amount * $request->count_reward_winners;

                if ($loggedUser->wallet >= $amountToDeduct) {
                    $loggedUser->wallet = $loggedUser->wallet - $amountToDeduct;
                    $loggedUser->save();
                } else{
                    return [
                        'status' => 'failed2',
                        'message' => 'Insufficient amount in wallet to reward users'
                    ];
                }
            }
            $request['user_id'] = $loggedUser->id;
            $request['cover_photo'] = $dbCoverImageName;
            $request['month_year'] = $month_year;
            $request['essay_description'] = '-';
            $request['post_writeup'] = nl2br($request->post_writeup);
            $request['question'] = json_encode($request->question);
            $request['option_a'] = json_encode($request->option_a);
            $request['option_b'] = json_encode($request->option_d);
            $request['option_c'] = json_encode($request->option_c);
            $request['option_d'] = json_encode($request->option_d);
            $request['answer'] = json_encode($request->answer);
            $request['game_permalink'] = strtolower(createPermalink($request->quiz_title));
            $request['tracking_key'] = $trackingKey;

            $request->except(['_token', 'cover_image']);

            $input = $request->all();
            QuizGames::create($input);

            return [
                'status' => 'success',
                'message' => 'Game created Successfully',
                'tracking_key' => $trackingKey
            ];
        } else {
            return [
                'status' => 'failed2',
                'message' => 'Unable to upload image at this time',
            ];
        }
    }

    public function createQuizSuccess($tracking_key) {
        $loggedUser = Sentinel::getUser();

        $game = QuizGames::where('tracking_key', $tracking_key)->firstOrFail();
        $gameId = $game->id;
        $gamePermalink = $game->game_permalink;

        return view('Dashboard.create_quiz_success', compact('gameId', 'gamePermalink', 'loggedUser'));
    }

    public function createEssaySuccess($tracking_key) {
        $loggedUser = Sentinel::getUser();

        $game = QuizGames::where('tracking_key', $tracking_key)->firstOrFail();
        $gameId = $game->id;
        $gamePermalink = $game->game_permalink;

        return view('Dashboard.create_essay_success', compact('gameId', 'gamePermalink', 'loggedUser'));
    }

    public function deleteGame($id) {
        $id = QuizGames::find($id);
        $id->delete();

        return [
            'status' => 'success',
            'message' => 'Campaign deleted Successfully'
        ];
    }

    public function closeGame($id) {
        $game = QuizGames::find($id);
        $game->game_closed = 1;
        $game->save();

        return [
            'status' => 'success',
            'message' => 'Game closed successfully'
        ];
    }

    public function openGame($id) {
        $game = QuizGames::find($id);
        $game->game_closed = 0;
        $game->save();

        return [
            'status' => 'success',
            'message' => 'Game closed successfully'
        ];
    }

    public function updateGame(Request $request, $id) {
        # Get the logged in User Id
        $loggedUser = Sentinel::getUser();
        $game = QuizGames::find($id);

        if ($loggedUser->id == $game->user_id) {

            $request['post_writeup'] = nl2br($request->post_writeup);
            $request['question'] = json_encode($request->question);
            $request['option_a'] = json_encode($request->option_a);
            $request['option_b'] = json_encode($request->option_d);
            $request['option_c'] = json_encode($request->option_c);
            $request['option_d'] = json_encode($request->option_d);
            $request['answer'] = json_encode($request->answer);
            $request['game_permalink'] = strtolower(createPermalink($request->quiz_title));

            $game->update($request->all());
            if ($game->save()) {
                return response()->json([
                    'status' => 'success',
                    'game' => $game
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'game' => $game
                ]);
            }
        }

    }

    public function updateEssayGame(Request $request, $id) {
        # Get the logged in User Id
        $loggedUser = Sentinel::getUser();
        $game = QuizGames::find($id);

        if ($loggedUser->id == $game->user_id) {

            $request['essay_title'] = $request->essay_title;
            $request['essay_description'] = nl2br($request->essay_description);
            $request['game_permalink'] = strtolower(createPermalink($request->essay_title));

            $game->update($request->all());
            if ($game->save()) {
                return response()->json([
                    'status' => 'success',
                    'game' => $game
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'game' => $game
                ]);
            }
        }

    }

    public function awardScore($win_user_id, $essay_id, $score) {
        $response = EssayResponse::where('win_user_id', $win_user_id)
                        ->where('essay_id', $essay_id)->first();

        $response->score = $score;
        $response->save();

        return [
            'status' => true,
            'data' => $response
        ];
    }
}
