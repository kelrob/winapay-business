<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RewardMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'hello@winapay.net';
        $subject = 'You have a new message';
        $name = 'Winapay Business';

        return $this->view('emails.reward_user')
            ->from($address, $name)
            ->cc($this->data['email'], $this->data['name'])
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([ 'text_message' => $this->data ]);
    }
}
