<?php

if (!function_exists('userData')) {
    function userData($id)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://winapay.com/api/user-profile?user_id=' . $id);
        $response = $request->getBody()->getContents();

        return json_decode($response);
    }
}


if (!function_exists('createPermalink')) {

    /**
     * @param $string           String value to create Permalink
     *
     * @return string|string[]|null     Return Permalink
     */
     function createPermalink($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
     }
}

if (!function_exists('randomString')) {

    function randomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

if (!function_exists('increaseWinUserWallet')) {

    function increaseWinUserWallet($userId, $amount) {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://winapay.com/api/increase-wallet?user_id=' . $userId . '&amount=' . $amount);
        $response = $request->getBody()->getContents();

        return json_decode($response);
    }

}

if (!function_exists('sendSms')) {

    function sendSms($receiverPhone, $message) {

        $prefix = '+';
        $str = $receiverPhone;

        if (substr($str, 0, strlen($prefix)) == $prefix) {
            $str = substr($str, strlen($prefix));
        }

        $url = 'http://www.mobileairtimeng.com/smsapi/bulksms.php';
        $fields = array(
            'username' => '08066781458',
            'password' => '166977e51dd11e79b3af2d',
            'message' => $message,
            'mobile' => $str,
            'sender' => 'WinApay',
            'normal route' => 1,
            'corporate route (Delivery to DND)' => 2,
            'vtype' => 1
        );

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return true;
    }

}

if (!function_exists('convertToEmbeddedUrl')) {
    function convertToEmbeddedUrl($youtubeUrl) {
        $convertedString = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe class=\"embed-responsive-item\"  src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$youtubeUrl);
        return $convertedString;
    }
}

if (!function_exists('approxFigure')) {
    function approxFigure($num) {

        if($num>1000) {

            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('k', 'm', 'b', 't');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;

        }

        return $num;
    }
}

if (!function_exists('paidSubscribers')) {
    function paidSubscribers($id)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://winapay.com/api/brand-paid-subscribers?brand_id=' . $id);
        $response = $request->getBody()->getContents();

        return json_decode($response);
    }
}