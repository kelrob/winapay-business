<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class User extends EloquentUser
{

    use Notifiable, HasApiTokens;

    protected $table = 'users';

    public function quizGames() {
        return $this->hasMany('App\Models\QuizGames');
    }

    public function withdrawal_request() {
        return $this->hasMany('App\Models\WithdrawalRequest');
    }

    public function fans() {
        return $this->hasMany('App\Models\Fan');
    }

    public function subscription() {
        return $this->hasOne('App\Models\Subscription');
    }

    public function brand_subscription_mode() {
        return $this->hasOne('App\Models\BrandSubscriptionMode');
    }
}
