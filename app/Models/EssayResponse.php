<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EssayResponse extends Model
{
    protected $table = 'essay_response';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function quizGames() {
        return $this->belongsTo('App\Models\QuizGames', 'essay_id');
    }
}
