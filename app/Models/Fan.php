<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fan extends Model
{
    protected $table = 'fans';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
