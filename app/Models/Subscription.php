<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
