<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizGames extends Model
{
    public $cover_image;
    protected $table = 'quiz_games';
    protected $primaryKey = 'id';

    protected $guarded = ['id', 'cover_image'];

    public function essay_response() {
        return $this->hasMany('App\Models\EssayResponse', 'essay_id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function quiz_response() {
        return $this->hasMany('App\Models\QuizResponse', 'game_id');
    }
}
