<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalRequest extends Model
{
    protected $table = 'withdrawal_request';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
