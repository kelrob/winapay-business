<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizResponse extends Model
{
    protected $table = 'quiz_response';
    protected $guarded = ['id'];

    public function quizGames() {
        return $this->belongsTo('App\Models\QuizGames', 'game_id');
    }
}
