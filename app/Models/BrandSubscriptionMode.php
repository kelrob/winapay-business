<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSubscriptionMode extends Model
{
    protected $table = 'brand_subscription_mode';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
