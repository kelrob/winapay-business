<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GamesPlayed extends Model
{
    public $cover_image;
    protected $table = 'games_played';
    protected $primaryKey = 'id';

}
