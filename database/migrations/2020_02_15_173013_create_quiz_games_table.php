<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('quiz_title')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('game_type')->nullable();
            $table->string('post_writeup')->nullable();
            $table->string('embedded_url')->nullable();
            $table->string('video_desc')->nullable();
            $table->text('question')->nullable();
            $table->string('option_a')->nullable();
            $table->string('option_b')->nullable();
            $table->string('option_c')->nullable();
            $table->string('option_d')->nullable();
            $table->string('answer')->nullable();
            $table->integer('time')->nullable();
            $table->string('fans_action')->nullable();
            $table->string('download_link')->nullable();
            $table->string('website_url')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('reward_type', 100)->nullable();
            $table->integer('count_reward_winners')->nullable();
            $table->integer('reward_amount')->nullable();
            $table->integer('gift_people_count')->nullable();
            $table->string('gift_product_name')->nullable();
            $table->string('game_category', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
