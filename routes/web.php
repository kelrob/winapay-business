<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
# Landing Page
Route::get('/', 'Pages\LandingPage@index');

# Authentication and Account Creation
Route::get('/sign-in', 'Security\LoginController@login')->name('signin');
Route::post('/sign-in', 'Security\LoginController@loginBusiness');
Route::get('/create-account', 'Security\LoginController@register')->name('create-account');
Route::post('/create-account', 'Security\LoginController@registerBusiness');
Route::get('/sign-out', 'Security\LoginController@logout');
Route::get('/forgot-password', 'Security\LoginController@forgotPassword');

# First Time Login
Route::get('/welcome', 'Pages\LandingPage@firstLogin')->middleware('business');
Route::get('/select-plan', 'Pages\LandingPage@selectPackage');

# Dashboard
Route::get('/dashboard', 'Pages\Dashboard@index')->middleware('business')->name('dashboard');

# Essay Response
Route::get('/essay-response', 'Pages\Dashboard@essayResponse')->middleware('business');
Route::get('/essay-response/info/{game_id}', 'Pages\Dashboard@essayResponseInfo')->middleware('business');
Route::get('/essay-response/details/{response_id}', 'Pages\Dashboard@responseDetails')->middleware('business');

// Campaign
Route::get('/create-campaign', 'Pages\Dashboard@createCampaign')->middleware('business');
Route::get('/create-campaign/quiz', 'Pages\Dashboard@createQuizCampaign')->middleware('business');
Route::get('/create-campaign/essay', 'Pages\Dashboard@createEssayCampaign')->middleware('business');
Route::post('/create-quiz-game', 'GameController@createQuizCampaign')->middleware('business');
Route::post('/create-essay-game', 'GameController@createEssayCampaign')->middleware('business');
Route::get('/create-quiz-success/{tracking_key}', 'GameController@createQuizSuccess')->middleware('business');
Route::get('/create-essay-success/{tracking_key}', 'GameController@createEssaySuccess')->middleware('business');
Route::get('/my-game/quiz/{game_id}/{game_permalink}', 'Pages\Dashboard@viewQuizGame')->middleware('business');
Route::get('/my-game/essay/{game_id}/{game_permalink}', 'Pages\Dashboard@viewEssayGame')->middleware('business');
Route::get('/edit-game/quiz/{game_id}/{game_permalink}', 'Pages\Dashboard@editQuizGame')->middleware('business');
Route::get('/edit-game/essay/{game_id}/{game_permalink}', 'Pages\Dashboard@editEssayGame')->middleware('business');
Route::delete('/delete/game/{id}', 'GameController@deleteGame')->middleware('business');
Route::put('/close/game/{id}', 'GameController@closeGame')->middleware('business');
Route::put('/open/game/{id}', 'GameController@openGame')->middleware('business');
Route::post('/update-quiz-game/{id}', 'GameController@updateGame')->middleware('business');
Route::post('/update-essay-game/{id}', 'GameController@updateEssayGame')->middleware('business');
Route::put('/award-score/{win_user_id}/{essay_id}/{score}', 'GameController@awardScore')->middleware('business');

# Statistics
Route::get('/statistics', 'Utils\Chart@index')->middleware('business');
//Route::get('/charts', 'Utils\Chart@index')->middleware('business');

# Profile
Route::get('/profile', 'UserController@index')->middleware('business');
Route::post('/update-profile', 'UserController@updateProfile')->middleware('business');
Route::post('/subscription/on', 'UserController@subscriptionOn')->middleware('business');
Route::put('/subscription/off', 'UserController@subscriptionOff')->middleware('business');

# Fund Wallet
Route::get('/fund-wallet', 'Pages\Dashboard@fundWallet')->middleware('business');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('/wallet/fund/{email}/{amountKobo}', 'PaymentController@increaseWallet');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

# Withdrawal Request
Route::post('/withdrawal-request', 'PaymentController@withdrawalRequest')->middleware('business');

# bank transfer Notification
Route::post('/bank-transfer',  'PaymentController@bankTransfer')->middleware('business');
Route::post('/bank-transfer',  'PaymentController@bankTransfer')->middleware('business');

# Leaderboard
Route::get('/leaderboard/quiz/{id}', 'LeaderboardController@quizLeaderboard')->middleware('business');
Route::get('/leaderboard/essay/{id}', 'LeaderboardController@essayLeaderboard')->middleware('business');

# Reward User
Route::put('/reward-user/{id}', 'LeaderboardController@rewardUser')->middleware('business');

# Change Logo
Route::post('change-logo', 'UserController@changeLogo')->middleware('business');

# Payment plan
Route::post('/rave-pay', 'RaveController@initialize')->name('rave-pay');
Route::get('/rave-pay', function() {
    return redirect(url('/select-plan'));
});
Route::get('/rave/callback', 'RaveController@callback')->name('callback');

# How it works
Route::get('/how-it-works', 'Pages\Dashboard@howItWorks')->middleware('business');

# Contact Us
Route::get('/contact-us', 'Pages\Dashboard@contactUs')->middleware('business');

/* Cron Jobs*/
# Count down to reduce the number of days left
Route::get('/countdown-plan-sub', 'CronJobController@countdownPlanSub');

# Delete from database if days_left <= 0
Route::get('/delete-plan-sub', 'CronJobController@deletePlanSub');

# Automatic Renewal of Plan Sub if money in wallet
Route::get('/renew-plan-sub', 'CronJobController@autoRenewPlanSub');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/send/email', 'Pages\Dashboard@mail');


Route::get('/test-email', 'UserController@testEmail');
