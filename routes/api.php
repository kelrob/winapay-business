<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
//Route::post('forgot-password', 'API\UserController@forgotPassword');

Route::get('brands/all/{limit}', 'API\UserController@allBrands');
Route::get('brand-games/all/{limit}', 'API\GameController@allBrandGames');
Route::get('brand-games/{brand_id}', 'API\GameController@brandGames');
Route::get('brand/{id}', 'API\UserController@userProfile');
Route::get('fetch-game/{game_id}', 'API\GameController@fetchGame');

Route::post('new-essay-response', 'API\GameController@newEssayResponse');
Route::post('new-quiz-response', 'API\GameController@newQuizResponse');
Route::post('discard-quiz-response', 'API\GameController@discardQuizResponse');

Route::get('essay-leaderboard/{id}', 'API\LeaderboardController@essayLeaderboard');
Route::get('quiz-leaderboard/{id}', 'API\LeaderboardController@quizLeaderboard');

Route::post('increase-wallet', 'API\UserController@increaseWallet');

Route::post('new-fan', 'API\UserController@newFan');

Route::post('game-played', 'API\GameController@newGamePlayed');

Route::get('played-quiz/{game_id}/{win_user_id}', 'API\LeaderboardController@userHasPlayedQuiz');

Route::get('try-again-expire/{win_user_id}/{game_id}', 'API\GameController@tryAgainExpire');
