$("#update-profile-form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "update-profile",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#update-spin').show();
            $('#update-btn').attr('disabled', 'disabled');
        },
        success: function (data) {

            if (data.status) {

                $('#update-spin').hide();
                $('#update-btn').attr('disabled', 'disabled').html('Profile Updated');

                toastr.success("Profile Updated Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("profile");
            } else {
                $('#update-spin').hide();
                $('#update-btn').removeAttr('disabled');

                toastr.error("Error Updating Profile. Please contact Admin","Error",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                })
            }
        }

    });
}));

$("#change-logo-form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "change-logo",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#logo-spin').show();
        },
        success: function (data) {
            console.log(data);
            $('#logo-spin').hide();


            if (data.status) {

                toastr.success("Logo Changed Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("profile");
            }
        }
    });

}));


$("#subscription-form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "subscription/on",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#update-s-spin').show();
            $('#update-s-btn').attr('disabled', 'disabled');
        },
        success: function (data) {

            if (data.status) {

                $('#update-s-spin').hide();
                $('#update-s-btn').attr('disabled', 'disabled').html('Subscription Updated');

                toastr.success("Subscription Updated Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("profile");
            } else {
                $('#update-s-spin').hide();
                $('#update-s-btn').removeAttr('disabled');

                toastr.error("Error Updating. Please contact Admin","Error",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                })
            }
        }

    });


}));

function offSubscription() {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "PUT",
        url: "subscription/off",
        beforeSend: function () {
            $('#btn-off-spin').show();
            $('#btn-off').attr('disabled', 'disabled');
        },
        success: function (data) {

            if (data.status) {

                $('#btn-off-spin').hide();
                $('#btn-off').attr('disabled', 'disabled').html('Subscription Updated');

                toastr.success("Subscription Updated Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("profile");
            } else {
                $('#btn-off-spin').hide();
                $('#btn-off').removeAttr('disabled');

                toastr.error("Error Updating. Please contact Admin","Error",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                })
            }
        }

    });
}