function deleteGame(id) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "DELETE",
        url: `../../../delete/game/${id}`,
        beforeSend: function () {
            $('#yes').attr("disabled","disabled");
            $('#del-loader').show();
        },
        success: function (response) {
            if (response.status == 'success') {

                toastr.success("Game deleted Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("../../../dashboard");
            }
        }
    });

}

function closeGame(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "PUT",
        url: `../../../close/game/${id}`,
        beforeSend: function () {
            $('#close-game-btn').attr("disabled","disabled");
            $('#close-game-loader').show();
        },
        success: function (response) {
            if (response.status == 'success') {

                toastr.success("Game Closed Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                let openBtn = `<a onclick="openGame(${id})" class="btn btn-sm btn-success" id="open-game-btn">
                                    Open game &nbsp;
                                    <i class="fa fa-spinner fa-spin" id="open-game-loader" style="display: none;"></i>
                                </a>`;

                $('#close-game-btn').removeAttr("disabled");
                $('#game-status').html(`<b style="color: #f44336">Closed</b>`);
                $('#close-open-btn').html(openBtn);
                $('#close-game-loader').hide();
            }
        }
    });
}

function openGame(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "PUT",
        url: `../../../open/game/${id}`,
        beforeSend: function () {
            $('#open-game-btn').attr("disabled","disabled");
            $('#open-game-loader').show();
        },
        success: function (response) {
            if (response.status == 'success') {

                toastr.success("Game Opened Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                let closeBtn = `<a onclick="closeGame(${id})" class="btn btn-sm btn-danger" id="close-game-btn">
                                    Close game &nbsp;
                                    <i class="fa fa-spinner fa-spin" id="close-game-loader" style="display: none;"></i>
                                </a>`;

                $('#open-game-btn').removeAttr("disabled");
                $('#close-open-btn').html(closeBtn);
                $('#game-status').html(`<b style="color: #11772d;">Opened</b>`);
                $('#open-game-loader').hide();
            }
        }
    });
}