$("#quiz_form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "../create-quiz-game",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#create-spin').show();
            $('#campaign-error').hide();
        },
        success: function (data) {
            console.log(data);
            let response = '';
            let errorList = data.errors;
            $('#create-spin').hide();
            if (data.res == 'failed') {
                $('#campaign-error').show();
                errorList.forEach(function (errors) {
                    $('#error_message').html(
                        response += `<li style="list-style-type: disc; line-height: 30px;">${errors}</li>`
                    );
                });

                toastr.error("Scroll up to view error","Error",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                })
            }

            if (data.status == 'failed2') {
                $('#campaign-error').show();
                $('#error_message').html(
                    response += `<li style="list-style-type: disc; line-height: 30px;">${data.message}</li>`
                );
                toastr.error("Scroll up to view error","Error",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                })
            }

            if (data.status == 'success') {

                toastr.success("Game Created Successfully","Success",{
                    timeOut:5e3,
                    closeButton:!0,
                    debug:!1,
                    newestOnTop:!0,
                    progressBar:!0,
                    positionClass:"toast-top-right",
                    preventDuplicates:!0,
                    onclick:null,
                    showDuration:"300",
                    hideDuration:"1000",
                    extendedTimeOut:"1000",
                    showEasing:"swing",
                    hideEasing:"linear",
                    showMethod:"fadeIn",
                    hideMethod:"fadeOut",
                    tapToDismiss:!1
                });

                window.location.replace("../create-quiz-success/" + data.tracking_key);
            }
        }
    });
}));
