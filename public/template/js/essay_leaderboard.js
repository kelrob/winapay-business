const rewardUser = (id) => {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "PUT",
        url: `../../../reward-user/${id}`,
        beforeSend: function () {
            $('#reward-text').show();
            $('#reward-response').hide();
            $('#reward_btn').attr('disabled', 'disabled').html('Please wait...').css('width', '20%');
        },
        success: function (data) {

            if (data.status) {
                $('#reward-text').hide();
                $('#reward_btn').removeAttr('disabled').html('Yes').css('width', '10%');

                let response = `<div class="alert alert-info" align="center">${data.data}</div>`;
                $('#reward-response').html(response).show();
            } else {
                $('#reward-text').hide();
                $('#reward_btn').removeAttr('disabled').html('Yes').css('width', '10%');

                let response = `<div class="alert alert-danger" align="center">${data.data}</div>`;
                $('#reward-response').html(response).show();
            }

        }

    });
};