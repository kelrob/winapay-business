function updateGame(game_id) {
    $("#update_form").unbind('submit').on('submit', (function(e) {
        e.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: `../../../update-quiz-game/${game_id}`,
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#spinner').show();
            },
            success: function (data) {
                console.log(data);
                if (data.status == 'success') {
                    toastr.success("Game Updated Successfully","Success",{
                        timeOut:5e3,
                        closeButton:!0,
                        debug:!1,
                        newestOnTop:!0,
                        progressBar:!0,
                        positionClass:"toast-top-right",
                        preventDuplicates:!0,
                        onclick:null,
                        showDuration:"300",
                        hideDuration:"1000",
                        extendedTimeOut:"1000",
                        showEasing:"swing",
                        hideEasing:"linear",
                        showMethod:"fadeIn",
                        hideMethod:"fadeOut",
                        tapToDismiss:!1
                    });
                }

                window.location.replace(`../../../my-game/quiz/${game_id}/${data.game.game_permalink}`);

            }
        });
    }));
}

function updateEssayGame(game_id) {
    $("#update_form").unbind('submit').on('submit', (function(e) {
        e.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: `../../../update-essay-game/${game_id}`,
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#spinner').show();
            },
            success: function (data) {
                console.log(data);
                if (data.status == 'success') {
                    toastr.success("Game Updated Successfully","Success",{
                        timeOut:5e3,
                        closeButton:!0,
                        debug:!1,
                        newestOnTop:!0,
                        progressBar:!0,
                        positionClass:"toast-top-right",
                        preventDuplicates:!0,
                        onclick:null,
                        showDuration:"300",
                        hideDuration:"1000",
                        extendedTimeOut:"1000",
                        showEasing:"swing",
                        hideEasing:"linear",
                        showMethod:"fadeIn",
                        hideMethod:"fadeOut",
                        tapToDismiss:!1
                    });
                }

                window.location.replace(`../../../my-game/essay/${game_id}/${data.game.game_permalink}`);

            }
        });
    }));
}