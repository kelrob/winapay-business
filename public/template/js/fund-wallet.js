$("#withdrawal-form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "withdrawal-request",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#withdraw-spin').show();
            $('#withdraw-btn').attr('disabled', 'disabled');
            $('#withdraw-text').show();
        },
        success: function (data) {

            if (data.status) {
                $('#withdraw-text').hide();
                $('#withdraw-spin').hide();
                $('#withdraw-btn').removeAttr('disabled');

                let response = `<div class="alert alert-info mt-3" align="center">${data.data}</div>`;
                $('#withdraw-response').html(response).show();

            } else {
                $('#withdraw-text').hide();
                $('#withdraw-spin').hide();
                $('#withdraw-btn').removeAttr('disabled');

                let response = `<div class="alert alert-danger mt-3" align="center">${data.data}</div>`;
                $('#withdraw-response').html(response).show();

            }
        }

    });


}));

$("#bank-transfer-form").unbind('submit').on('submit', (function(e) {
    e.preventDefault();

    let depositorName = $('#depositor_name').val();
    let amountPaid = $('#amount_paid').val();
    let paymentDate = $('#payment_date').val();


    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "bank-transfer",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#bank-transfer-spin').show();
            $('#bank-transfer-btn').attr('disabled', 'disabled');
            $('#bank-transfer-text').show();
        },
        success: function (data) {

            if (data.status) {
                $('#bank-transfer-text').hide();
                $('#bank-transfer-spin').hide();
                $('#bank-transfer-btn').removeAttr('disabled');

                alert('Okay');

                let response = `<div class="alert alert-info mt-3" align="center">${data.data}</div>`;
                $('#bank-transfer-response').html(response).show();



            }
        }

    });



}));
