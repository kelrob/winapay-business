function awardScore(win_user_id, essay_id) {
    let score = document.getElementById("range").value;
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "PUT",
        url: `../../award-score/${win_user_id}/${essay_id}/${score}`,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('#submit-loader').show();
            $('#btn-text').html('Submitting');
        },
        success: function (data) {
            if (data.status) {
                $('#submit-loader').hide();
                $('#btn-text').html('Score Awarded');
            }

            toastr.success("Score Awarded to user successfully","Success",{
                timeOut:5e3,
                closeButton:!0,
                debug:!1,
                newestOnTop:!0,
                progressBar:!0,
                positionClass:"toast-top-right",
                preventDuplicates:!0,
                onclick:null,
                showDuration:"300",
                hideDuration:"1000",
                extendedTimeOut:"1000",
                showEasing:"swing",
                hideEasing:"linear",
                showMethod:"fadeIn",
                hideMethod:"fadeOut",
                tapToDismiss:!1
            });

            window.location.replace("../../essay-response");
        }
    });

}