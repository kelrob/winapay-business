@extends('security.template')

@section('form')
    <section>
        <div class="form">
            <h6>Password Reset</h6>
            <h3 style="color: #222">Forgot Paassword?</h3>

            <div class="form-field">

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form autocomplete="off" method="post" action="{{ route('password.email') }}">

                    {{ @csrf_field() }}


                    <div class="form-group">
                        <label for="email">Email</label>
                        <input
                            type="text"
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}}"
                            id="email"
                            name="email"
                            value="{{ old('email') }}"
                        />
                        @if ( $errors->has('email') )
                            <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-customize" value="Send Password Reset Link">
                    </div>
                    <div class="form-group">
                        <p>
                            <small>
                                Remember Password?
                                <b style="text-decoration: none">
                                    <a href="{{ url('sign-in') }}">Login here</a>
                                </b>
                            </small>
                        </p>
                    </div>
                </form>
            </div>

        </div>
    </section>
@endsection
