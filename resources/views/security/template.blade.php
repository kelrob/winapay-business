<!DOCTYPE html>
<html lang="zxx">

<!-- =====================================
====Information on the Head Tag -->
<head>

    <!--- Basic page needs
   ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <!--- Extra Meta Data Information
   ================================================== -->
    <meta name="robots" content="index, follow">
    <meta name="keywords"
          content="Gamify, Knowledge, Education, Games, Learning, Trivia Quiz, Make money online, Online game, Create Quiz, Free game, Paid game, Edutech, Game App, Play Online Game, FastPlay, Kahoot"/>
    <meta name="description"
          content="WinApay is an online gaming Platform that makes learning Fun for everyone. Create Quiz, Play Games, Challenge Friends and get Rewarded."/>
    <meta name="author" content="WinApay Limited"/>
    <meta name="theme-color" content="#1976d2">

    <!--- Title
   ================================================== -->
    <title>WINAPAY - Get Started</title>

    <!--- FavIcon
   ================================================== -->
    <link rel="shortcut icon" href="{{ url('img/favicon.ico') }}"/>

    <!--- Google Fonts
   ================================================== -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">

    <!--- Font Awesome
   ================================================== -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!--- Plugins and Core Style CSS
   ================================================== -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">

    <!--- WinApay Custom Css
   ================================================== -->
    <link rel="stylesheet" href="{{ url('styles/custom.css') }}"/>

    <!--- External Libraries
   ================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123552603-1"></script>

    <!--- Mandatory Styling
   ================================================== -->
</head>

<body>

<div class="container">
    <div class="row" id="second-row">
        <div class="col-lg-6">
            <a href="{{ url('sign-in') }}">
                <img src="https://winapay.com/img/logo-light.png" id="native-logo" alt="logo"/>
            </a>
        </div>
        <div class="col-lg-6">
            @yield('form')
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="{{ url('js/jquery-3.0.0.min.js') }}"></script>
<script src="{{ url('js/jquery-migrate-3.0.0.min.js') }}"></script>

<!-- bootstrap -->
<script src="{{ url('js/bootstrap.min.js') }}"></script>

</body>
</html>
