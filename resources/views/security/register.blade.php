@extends('security.template');

@section('form')
    <section>
        <div class="form">
            <h6>GO BACK TO WINAPAY &rarr;</h6>
            <h3 style="color: #222">Create a Business Account</h3>

            <div class="form-field">
                <form autocomplete="off" method="post" action="{{ url('/create-account') }}">
                    {{ csrf_field() }}

                    @if($errors->any())
                        <div class="alert alert-danger" style="padding-bottom: 0;">
                            <p>{{$errors->first()}}</p>
                        </div>
                    @endif

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="business_name">Business Name</label>
                            <input
                                    type="text"
                                    name="business_name"
                                    class="form-control{{ $errors->has('business_name') ? ' is-invalid': '' }} "
                                    id="business_name"
                                    value="{{ old('business_name') }}"
                            />
                            @if ( $errors->has('business_name') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('business_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email">Business Email</label>
                            <input
                                    type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid': '' }}"
                                    id="email"
                                    name="email"
                                    value="{{ old('email') }}"
                            />
                            @if ( $errors->has('email') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="phone">Business Phone</label>
                            <input type="text"
                                   name="phone"
                                   class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                   id="phone"
                                   value="{{ old('phone') }}"
                            />
                            @if ( $errors->has('phone') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="category">Business Category</label>
                            <input
                                    type="text"
                                    name="category"
                                    id="category"
                                    class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}"
                                    value="{{ old('category') }}"
                            />
                            @if ( $errors->has('category') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('category') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="password">Password</label>
                            <input
                                    type="password"
                                    name="password"
                                    class="form-control{{ $errors->first('password') ? ' is-invalid' : '' }}"
                                    id="password"
                            />
                            @if ( $errors->has('password') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password_again">Password Again</label>
                            <input
                                    type="password"
                                    name="password_again"
                                    class="form-control{{ $errors->first('password_again') ? ' is-invalid' : ''}}"
                                    id="password_again"
                            />
                            @if ( $errors->has('password_again') )
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password_again') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-customize" value="CREATE ACCOUNT">
                    </div>
                    <div class="form-group">
                        <p>Already have an account?
                            <b style="text-decoration: none">
                                <a href="{{ url('sign-in') }}">Login!</a>
                            </b>
                        </p>
                    </div>
                </form>
            </div>

        </div>
    </section>
@endsection
