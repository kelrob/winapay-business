@extends('security.template')

@section('form')
    <section>
        <div class="form">
            <h6>GO BACK TO WINAPAY &rarr;</h6>
            <h3 style="color: #222">Login to Winapay for Business</h3>

            <div class="form-field">

                <form autocomplete="off" method="post" action="{{ url('sign-in') }}">

                    {{ @csrf_field() }}

                    @if($errors->any())
                        <div class="alert alert-danger" style="padding-bottom: 0;">
                            <p>{{$errors->first()}}</p>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input
                                type="text"
                                class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}}"
                                id="email"
                                name="email"
                                value="{{ old('email') }}"
                        />
                        @if ( $errors->has('email') )
                            <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input
                                type="password"
                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                id="password"
                                name="password"
                        />
                        @if ( $errors->has('password') )
                            <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <p><small>Forgot your password? <b><a href="{{ url('forgot-password') }}" style="color: #222;">Click here</a></b></small></p>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-customize" value="LOGIN">
                    </div>
                    <div class="form-group">
                        <p>Don't have an account?
                            <b style="text-decoration: none">
                                <a href="{{ url('create-account') }}">Create One</a>
                            </b>
                        </p>
                    </div>
                </form>
            </div>

        </div>
    </section>
@endsection
