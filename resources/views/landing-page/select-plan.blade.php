<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WINAPAY PLAN</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="assets2/css/bootstrap.min.css">

    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="assets2/css/main.css">

    <!--Icon Fonts-->
    <link rel="stylesheet" media="screen" href="assets2/fonts/font-awesome/font-awesome.min.css" />

</head>

<body>
<!-- Pricing Table Section -->
<section id="pricing-table">
    <div class="container">
        <div class="row">
            <div class="col-lg-12" align="center">
                <img src="https://winapay.com/img/logo-dark.png" style="width: 100px; height: 32px;">
            </div>
            <div class="pricing" style="margin-top: 6%;">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing-table">
                        <div class="pricing-header">
                            <p class="pricing-title">Starter Plan</p>
                            <p class="pricing-rate"><sup>&#8358;</sup> 2,000 <span>/Month.</span></p>
                        </div>

                        <div class="pricing-list" align="center">
                            <ul>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Automated sms and mail to winners</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Seamless payment to Users</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Quizzes</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Players</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Quiz Campaigns</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Essay Campaigns</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Online Payment</li>
                            </ul>

                            @php
                                $array = array(array('metaname' => 'type', 'metavalue' => 'business_plan'));
                            @endphp
                            <form method="POST" action="{{ route('rave-pay') }}" id="paymentForm">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount" value="2000" /> <!-- Replace the value with your transaction amount -->
                                <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                                <input type="hidden" name="description" value="Winapay Business Starter Plan" /> <!-- Replace the value with your transaction description -->
                                <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                                <input type="hidden" name="currency" value="NGN" /> <!-- Replace the value with your transaction currency -->
                                <input type="hidden" name="email" value="{{ $profile->email }}" /> <!-- Replace the value with your customer email -->
                                <input type="hidden" name="firstname" value="{{ $profile->business_name }}" /> <!-- Replace the value with your customer firstname -->
                                <input type="hidden" name="lastname" value="" /> <!-- Replace the value with your customer lastname -->
                                <input type="hidden" name="metadata" value="{{ json_encode($array) }}" > <!-- Meta data that might be needed to be passed to the Rave Payment Gateway -->
                                <input type="hidden" name="phonenumber" value="" /> <!-- Replace the value with your customer phonenumber -->
                                {{-- <input type="hidden" name="paymentplan" value="362" /> <!-- Ucomment and Replace the value with the payment plan id --> --}}
                                {{-- <input type="hidden" name="ref" value="MY_NAME_5uwh2a2a7f270ac98" /> <!-- Ucomment and  Replace the value with your transaction reference. It must be unique per transaction. You can delete this line if you want one to be generated for you. --> --}}
                                {{-- <input type="hidden" name="logo" value="https://pbs.twimg.com/profile_images/915859962554929153/jnVxGxVj.jpg" /> <!-- Replace the value with your logo url (Optional, present in .env)--> --}}
                                {{-- <input type="hidden" name="title" value="Flamez Co" /> <!-- Replace the value with your transaction title (Optional, present in .env) --> --}}
                                <input type="submit"
                                       class="btn btn-custom btn-rounded"
                                       style="background-color: #34495e; color: #fff; border-style: none; border-radius: 30px; width: 80%;"
                                       value="Select Plan"  />

                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing-table">
                        <div class="pricing-header">
                            <p class="pricing-title">Business Plan</p>
                            <p class="pricing-rate"><sup>&#8358;</sup> 10,000 <span>/6 Month.</span></p>
                        </div>

                        <div class="pricing-list" align="center" style="border: 1px solid #34495e;">
                            <ul>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Automated sms and mail to winners</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Seamless payment to Users</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Quizzes</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Players</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Quiz Campaigns</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Essay Payment</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Online Payment</li>
                            </ul>
                            @php
                                $array = array(array('metaname' => 'type', 'metavalue' => 'business_plan'));
                            @endphp
                            <form method="POST" action="{{ route('rave-pay') }}" id="paymentForm">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount" value="10000" /> <!-- Replace the value with your transaction amount -->
                                <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                                <input type="hidden" name="description" value="Winapay Business Starter Plan" /> <!-- Replace the value with your transaction description -->
                                <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                                <input type="hidden" name="currency" value="NGN" /> <!-- Replace the value with your transaction currency -->
                                <input type="hidden" name="email" value="{{ $profile->email }}" /> <!-- Replace the value with your customer email -->
                                <input type="hidden" name="firstname" value="{{ $profile->business_name }}" /> <!-- Replace the value with your customer firstname -->
                                <input type="hidden" name="lastname" value="" /> <!-- Replace the value with your customer lastname -->
                                <input type="hidden" name="metadata" value="{{ json_encode($array) }}" > <!-- Meta data that might be needed to be passed to the Rave Payment Gateway -->
                                <input type="hidden" name="phonenumber" value="" /> <!-- Replace the value with your customer phonenumber -->
                                {{-- <input type="hidden" name="paymentplan" value="362" /> <!-- Ucomment and Replace the value with the payment plan id --> --}}
                                {{-- <input type="hidden" name="ref" value="MY_NAME_5uwh2a2a7f270ac98" /> <!-- Ucomment and  Replace the value with your transaction reference. It must be unique per transaction. You can delete this line if you want one to be generated for you. --> --}}
                                {{-- <input type="hidden" name="logo" value="https://pbs.twimg.com/profile_images/915859962554929153/jnVxGxVj.jpg" /> <!-- Replace the value with your logo url (Optional, present in .env)--> --}}
                                {{-- <input type="hidden" name="title" value="Flamez Co" /> <!-- Replace the value with your transaction title (Optional, present in .env) --> --}}
                                <input type="submit"
                                       class="btn btn-custom btn-rounded"
                                       style="background-color: #34495e; color: #fff; border-style: none; border-radius: 30px; width: 80%;"
                                       value="Select Plan"  />

                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="pricing-table">
                        <div class="pricing-header">
                            <p class="pricing-title">One Time Plan</p>
                            <p class="pricing-rate"><sup>&#8358;</sup> 18,000 <span>/Year.</span></p>
                        </div>

                        <div class="pricing-list" align="center">
                            <ul>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Automated sms and mail to winners</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Seamless payment to Users</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Quizzes</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Unlimited Players</li>
                                <li style="color: #222;"><i class="fa fa-check" style="color: #222"></i>Quiz Campaigns</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Essay Campaigns</li>
                                <li style="color: #ccc; text-decoration: line-through;"><i class="fa fa-times" style="color: #ccc"></i>Online Campaigns</li>
                            </ul>
                            @php
                                $array = array(array('metaname' => 'type', 'metavalue' => 'business_plan'));
                            @endphp
                            <form method="POST" action="{{ route('rave-pay') }}" id="paymentForm">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount" value="18000" /> <!-- Replace the value with your transaction amount -->
                                <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                                <input type="hidden" name="description" value="Winapay Business Starter Plan" /> <!-- Replace the value with your transaction description -->
                                <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                                <input type="hidden" name="currency" value="NGN" /> <!-- Replace the value with your transaction currency -->
                                <input type="hidden" name="email" value="{{ $profile->email }}" /> <!-- Replace the value with your customer email -->
                                <input type="hidden" name="firstname" value="{{ $profile->business_name }}" /> <!-- Replace the value with your customer firstname -->
                                <input type="hidden" name="lastname" value="" /> <!-- Replace the value with your customer lastname -->
                                <input type="hidden" name="metadata" value="{{ json_encode($array) }}" > <!-- Meta data that might be needed to be passed to the Rave Payment Gateway -->
                                <input type="hidden" name="phonenumber" value="" /> <!-- Replace the value with your customer phonenumber -->
                                {{-- <input type="hidden" name="paymentplan" value="362" /> <!-- Ucomment and Replace the value with the payment plan id --> --}}
                                {{-- <input type="hidden" name="ref" value="MY_NAME_5uwh2a2a7f270ac98" /> <!-- Ucomment and  Replace the value with your transaction reference. It must be unique per transaction. You can delete this line if you want one to be generated for you. --> --}}
                                {{-- <input type="hidden" name="logo" value="https://pbs.twimg.com/profile_images/915859962554929153/jnVxGxVj.jpg" /> <!-- Replace the value with your logo url (Optional, present in .env)--> --}}
                                {{-- <input type="hidden" name="title" value="Flamez Co" /> <!-- Replace the value with your transaction title (Optional, present in .env) --> --}}
                                <input type="submit"
                                       class="btn btn-custom btn-rounded"
                                       style="background-color: #34495e; color: #fff; border-style: none; border-radius: 30px; width: 80%;"
                                       value="Select Plan"  />

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Table Section End -->
</body>
</html>
