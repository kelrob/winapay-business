<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Winapay For Business</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ url('img/favicon.ico') }}"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Animiate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5eac867d203e206707f8bbe0/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex">

        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.html">WinApay<span>.</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="#header">Home</a></li>
                <li><a href="#about">How it works</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#pricing">Pricing</a></li>
                {{--<li><a href="#portfolio">Portfolio</a></li>--}}
                {{--<li><a href="#team">Team</a></li>--}}
                {{--<li class="drop-down"><a href="">Drop Down</a>--}}
                    {{--<ul>--}}
                        {{--<li><a href="#">Drop Down 1</a></li>--}}
                        {{--<li class="drop-down"><a href="#">Drop Down 2</a>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Deep Drop Down 1</a></li>--}}
                                {{--<li><a href="#">Deep Drop Down 2</a></li>--}}
                                {{--<li><a href="#">Deep Drop Down 3</a></li>--}}
                                {{--<li><a href="#">Deep Drop Down 4</a></li>--}}
                                {{--<li><a href="#">Deep Drop Down 5</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Drop Down 3</a></li>--}}
                        {{--<li><a href="#">Drop Down 4</a></li>--}}
                        {{--<li><a href="#">Drop Down 5</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a href="#contact">Contact Us</a></li>--}}

                <li class="get-started"><a href="{{ url('sign-in') }}">Get Started</a></li>
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">

    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="text-center col-lg-6 py-5 py-lg-0 order-2 order-lg-1" data-aos="fade-right">
                <h1>Your Digital Reward App For Fans & Customers</h1>
                <h2>Ready to reward loyal fans & customers using quizzes and essay forms?</h2>
                <a href="{{ url('sign-in') }}" class="btn-get-started">Get Started</a>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
                <img src="assets/img/hero-img.png" class="img-fluid animated rollIn duration-2s delay-2s" alt="">
            </div>
        </div>
    </div>

</section><!-- End Hero -->

<main id="main">

    <!-- ======= Clients Section =======
    <section id="clients" class="clients section-bg">
        <div class="container">

            <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-1.png" class="img-fluid" alt="" data-aos="flip-right">
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-2.png" class="img-fluid" alt="" data-aos="flip-right" data-aos-delay="100">
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-3.png" class="img-fluid" alt="" data-aos="flip-right" data-aos-delay="200">
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-4.png" class="img-fluid" alt="" data-aos="flip-right" data-aos-delay="300">
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-5.png" class="img-fluid" alt="" data-aos="flip-right" data-aos-delay="400">
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="client-logo">
                        <img src="assets/img/clients/client-6.png" class="img-fluid" alt="" data-aos="flip-right" data-aos-delay="500">
                    </div>
                </div>

            </div>

        </div>
    </section> End Clients Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
        <div class="container">

            <div class="row">
                <div class="col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start">
                    <div class="content d-flex flex-column">
                        <h3 data-aos="fade-in" data-aos-delay="100">How it works?</h3>
                        <P>
                            Watch how to use winapay to reward your fans or customers
                            seamlessly
                        </P>
                        <div class="embed-responsive embed-responsive-16by9 mt-5">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/XaJdiypMSN8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 pl-lg-5 pr-lg-1 d-flex align-items-stretch">
                    <div class="content d-flex flex-column justify-content-center">
                        <h3 data-aos="fade-in" data-aos-delay="100">Start rewarding your loyal fans and customers now</h3>
                        <p data-aos="fade-in">
                            With just one step away, you are close to rewarding your loyal fans and customers with ease.
                        </p>
                        <div class="row">
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                                <i class="bx bx-share"></i>
                                <h4><a href="#">Fun and Sharable</a></h4>
                                <p>Make loyal fans or customers happy, by making Quizzes and Giveaway that are shareable to those they love.</p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                                <i class="bx bx-wind"></i>
                                <h4><a href="#">Seamless Rewarding</a></h4>
                                <p>With just the click of a button, selected fans or customers are rewarded!</p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                                <i class="bx bx-layer"></i>
                                <h4><a href="#">Monetizing via Subscription</a></h4>
                                <p>Choose either to engage fans on a weekly basis through subscription Monetization.
                                </p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up">
                                <i class="bx bx-time"></i>
                                <h4><a href="#">Track Engagement in Real Time</a></h4>
                                <p>Forget about the stress of calculating and analyzing winners.
                                    Brands, Customers & fans can see Ranking activities in real-time, which brings credibility to your campaign from the customer viewpoint.</p>
                            </div>
                        </div>
                    </div><!-- End .content-->
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-in">Services</h2>
                <p data-aos="fade-in">It doesnt matter who you are or what you do, we got a spot for you. <br />Get Your customers and fans closer with our Automated quiz and essay form without the need to manually reward customers.</p>
            </div>

            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-right">
                    <div class="card">
                        <div class="card-img">
                            <img src="img/business2.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">For Businesses</a></h5>
                            <p class="card-text text-center">Get your customers aware of new product or services by engaging them with fun Quizzes and seamless automation that reward winners.</p>
                            {{--<div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-left">
                    <div class="card">
                        <div class="card-img">
                            <img src="img/celebrity.jpg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">For Celebrities</a></h5>
                            <p class="card-text text-center">No better way to get your fans engaged weekly than organizing Quizzes which keeps them engaged with you on social media, using a Giveaway or free ticket.</p>
                            {{--<div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-right">
                    <div class="card">
                        <div class="card-img">
                            <img src="img/blog.jpg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">For Bloggers</a></h5>
                            <p class="card-text text-center">Keep your blog visitors engaged with your Content from your website using automated Quizzes.
                                <br />
                                In addition, monetize your blog using winapay for every quiz taken by web visitors from your blog.</p>
                            {{--<div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-left">
                    <div class="card">
                        <div class="card-img">
                            <img src="img/influ.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">For Influencers</a></h5>
                            <p class="card-text text-center">Looking for ways to do Giveaway to fans on social media with Quizzes or Essay form, then look no further.

                                Create your Quizzes or Essay in minutes and share with loyal Fans</p>
                            {{--<div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Services Section -->


    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing section-bg">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-in">Pricing</h2>
                <p data-aos="fade-in">Choose The Best Pricing Plan That Suit Your Brand Needs.<br />
                    Not Sure What Plan to Choose? Try <b>Our One Time Plan</b> and get complete access with low cost.</p>
            </div>

            <div class="row no-gutters">

                <div class="col-lg-4 box" data-aos="zoom-in">
                    <h3>Starter</h3>
                    <h4>&#8358;2,000<span>per month</span></h4>
                    <ul class="text-center">
                        <li><i class="bx bx-check"></i> Automated sms and mail to winners</li>
                        <li><i class="bx bx-check"></i> Seamless payment to Users</li>
                        <li><i class="bx bx-check"></i> Unlimited Quizzes</li>
                        <li><i class="bx bx-check"></i> Unlimited Players</li>
                        <li><i class="bx bx-check"></i> Quiz Campaigns</li>
                        <li><i class="bx bx-check"></i> Essay Campaigns</li>
                        <li><i class="bx bx-check"></i> Online Payments</li>
                    </ul>
                    <a href="{{ route('create-account') }}" class="get-started-btn">Get Started</a>
                </div>

                <div class="col-lg-4 box featured" data-aos="zoom-in">
                    <span class="featured-badge">Featured</span>
                    <h3>Business</h3>
                    <h4>&#8358;10,000<span>6 months</span></h4>
                    <ul class="text-center">
                        <li><i class="bx bx-check"></i> Automated sms and mail to winners</li>
                        <li><i class="bx bx-check"></i> Seamless payment to Users</li>
                        <li><i class="bx bx-check"></i> Unlimited Quizzes</li>
                        <li><i class="bx bx-check"></i> Unlimited Players</li>
                        <li><i class="bx bx-check"></i> Quiz Campaigns</li>
                        <li><i class="bx bx-check"></i> Essay Campaigns</li>
                        <li><i class="bx bx-check"></i> Online Payments</li>
                    </ul>
                    <a href="{{ route('create-account') }}" class="get-started-btn">Get Started</a>
                </div>

                <div class="col-lg-4 box" data-aos="zoom-in">
                    <h3>Per year</h3>
                    <h4>&#8358;18,000<span>12 Months</span></h4>
                    <ul class="text-center">
                        <li><i class="bx bx-check"></i> Automated sms and mail to winners</li>
                        <li><i class="bx bx-check"></i> Seamless payment to Users</li>
                        <li><i class="bx bx-check"></i> Unlimited Quizzes</li>
                        <li><i class="bx bx-check"></i> Unlimited Players</li>
                        <li><i class="bx bx-check"></i> Quiz Campaigns</li>
                        <li><i class="bx bx-check"></i> Essay Campaigns</li>
                        <li><i class="bx bx-check"></i> Online Payments</li>
                    </ul>
                    <a href="{{ route('create-account') }}" class="get-started-btn">Get Started</a>
                </div>

            </div>

        </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-in">Frequently Asked Questions</h2>
                {{--<p data-aos="fade-in">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>--}}
            </div>

            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up">
                <div class="col-lg-5">
                    <i class="bx bx-help-circle"></i>
                    <h4>What Kind Of Reward Can be given out?</h4>
                </div>
                <div class="col-lg-7">
                    <p>
                        Reward is solely base on Brand purpose. It could be Gift, Ticket, Product Or Cash.
                        For Cash Giveaway, winapay enables brand fund wallet, while we reward customers based on brand specification and number of expected winners.
                        <br />
                        For Rewards that are not cash, we automatically notified winners with a Direct Link to contact Brand

                    </p>
                </div>
            </div><!-- End F.A.Q Item-->

            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-5">
                    <i class="bx bx-help-circle"></i>
                    <h4> How Do We Integrate Our Quizzes from Winapay to our website or social media account?</h4>
                </div>
                <div class="col-lg-7">
                    <p>
                        Upon creation of Quiz or Essay Campaign, You Direct Fans Or customers to your CAMPAIGN via a Campaign link.
                        All information and answer by Fans or Customer are submitted in Real-Time to your Dashboard.
                    </p>
                </div>
            </div><!-- End F.A.Q Item-->

            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                <div class="col-lg-5">
                    <i class="bx bx-help-circle"></i>
                    <h4>Can Quizzes Be Monetize?</h4>
                </div>
                <div class="col-lg-7">
                    <p>
                        Yes. It depends on brand purpose which can be turned On with just a click, while winapay handles the rest on a 20% commission per subscriber.
                    </p>
                </div>
            </div><!-- End F.A.Q Item-->

            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                <div class="col-lg-5">
                    <i class="bx bx-help-circle"></i>
                    <h4>Do you have a Support Line where we can reach You?
                    </h4>
                </div>
                <div class="col-lg-7">
                    <p>
                        Yes. You can contact us <a href="mailto:info@winapay.com">info@winapay.com</a>

                        Or
                        Message us via the live chat.
                    </p>
                </div>
            </div><!-- End F.A.Q Item-->

            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
                <div class="col-lg-5">
                    <i class="bx bx-help-circle"></i>
                    <h4>Must we subscribe to a Plan Before Gaining access to our Account?</h4>
                </div>
                <div class="col-lg-7">
                    <p>
                        Yes. To avoid unsolicited accounts. All brand are to subscribe to a plan to gain Access.
                    </p>
                </div>
            </div><!-- End F.A.Q Item-->

        </div>
    </section><!-- End Frequently Asked Questions Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">

        <div class="container" data-aos="fade-up">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <h3>Our Products</h3>
                    <p style="font-style: normal;">
                        <a href="#">Winapay for fun</a><br><br />
                        <a href="#">Winapay for Schools</a><br>
                    </p>
                    <h4 class="mt-5 text-bold">Contact Us</h4>
                    <p class="mt-4" style="font-style: normal;">
                        <i class="bx bx-phone"></i> &nbsp;+234 705 9845 923
                        <br /> <br />
                        <i class="bx bx-envelope"></i> &nbsp;<a href="mailto:info@winapay.com">info@winapay.com</a> </p>
                </div>
                <div class="col-lg-6 text-left">
                    <h3>WINAPAY  <small style="font-weight: 100;"> | Business</small></h3>
                    <p>I
                        t doesnt matter who you are or what you do, we got a spot for you.
                        Get Your customers and fans closer with our Automated quiz and essay form without the need to manually reward customers.</p>

                    <div class="social-links pull-right">
                        <a href="https://twitter.com/my_winapay" target="_blank" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="https://www.facebook.com/mywinapay" target="_blank" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="https://www.instagram.com/winapay" target="_blank" class="instagram"><i class="bx bxl-instagram"></i></a>
                    </div>
                </div>
            </div>

            <!--
            <div class="row footer-newsletter justify-content-center">
                <div class="col-lg-6">
                    <form action="" method="post">
                        <input type="email" name="email" placeholder="Enter your Email"><input type="submit" value="Subscribe">
                    </form>
                </div>
            </div>
            -->


        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>WINAPAY</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <p>
                <a href="https://winapay.com/privacy-policy">Privacy Policy</a> |
                <a href="https://winapay.com/terms-conditions">Terms and Condition</a>
            </p>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/venobox/venobox.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
