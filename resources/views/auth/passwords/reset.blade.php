@extends('security.template')

@section('form')
    <section>
        <div class="form">
            <h6>Reset Password</h6>
            <h3 style="color: #222">Reset your password</h3>

            <div class="form-field">

                <form autocomplete="off" method="post" action="{{ route('password.update') }}">

                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">


                    <div class="form-group">
                        <label for="email">Email</label>
                        <input
                            type="text"
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}}"
                            id="email"
                            name="email"
                            value="{{ old('email') }}"
                            required autofocus
                        />
                        @if ( $errors->has('email') )
                            <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input
                            type="password"
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                            id="password"
                            name="password"
                            required
                        />
                        @if ( $errors->has('password') )
                            <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password_confirm">Confirm Password</label>
                        <input
                            type="password"
                            class="form-control"
                            id="password_confirm"
                            name="password_confirmation"
                            required
                        />
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-customize" value="Reset Password">
                    </div>
                </form>
            </div>

        </div>
    </section>
@endsection
