@extends('Dashboard.template')

@section('section-main')
    <section>
        <div class="row" style="background-color: #fff; padding: 2%;">
            <div class="col-lg-12">
                <h6 style="font-weight: 500; letter-spacing: 1px;">All Responses to
                    <span style="color: #2e6da4">
                        {{ $essay_title }}
                    </span>
                </h6>
                <hr class="" />
                <div class="table-responsive">
                    <table class="table header-border table-hover verticle-middle">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Score Awarded</th>
                            <th scope="col">Action</th>
                            <th scope="col">Time Submitted</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($responses as $response)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ userData($response->win_user_id)->fullname }}</td>
                                    <td>
                                        @if($response->score == 0)
                                            <b class="text-danger">No Score Awarded</b>
                                            @else
                                            <b class="text-info">{{ $response->score }} / 100</b>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('essay-response/details/' . $response->id) }}" class="btn btn-info btn-sm">View Response</a>
                                    </td>
                                    <td>{{ $response->created_at->format('M d, Y - H:i:s') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection