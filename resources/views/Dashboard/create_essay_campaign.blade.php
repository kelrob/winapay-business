@extends('Dashboard.template')
@section('section-main')
    <div class="row mt-4">
        <div class="col-md-8 offset-md-2">
            <div class="container">
                <div class="stepwizard ">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step col-xs-3">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p><small>Quiz Information</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-3">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p><small>Questions & Answers</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-3">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p><small>Finish</small></p>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" id="campaign-error" style="margin-bottom: 2%; display: none; padding-left: 4%;">
                    <ul style="list-style-type: circle; padding-left: 4%" id="error_message"></ul>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form role="form" id="quiz_form" enctype="multipart/form-data">
                            @csrf
                            <div class="panel panel-primary setup-content" id="step-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center ">Essay Information</h3>
                                    <h4 class="text-center"><small></small></h4>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label" for="title">Title</label>
                                        <input maxlength="100" type="text" name="essay_title" class="form-control" placeholder="Enter Essay Title" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="cover-image">Cover Image</label>
                                        <input type="file" name="cover_image" class="form-control" placeholder="Enter Last Name" />
                                    </div>

                                    <p align="center"><button class="btn btn-primary nextBtn custom-btn" type="button">Next</button></p>
                                </div>
                            </div>

                            <div class="panel panel-primary setup-content" id="step-2">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">Essay Description</h3>
                                    <h4 class="text-center"><small></small></h4>
                                </div>
                                <div class="panel-body" id="qPanel">
                                    <div class="form-group">
                                        <label class="control-label" for="essay_description">Essay Description</label>
                                        <textarea class="form-control" id="essay_description" rows="5" cols="10" maxlength="1800" name="essay_description" placeholder="Give a description of what you want your audience to write about."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="char_no">Total number of Characters</label>
                                        <input type="number" name="character_no" class="form-control" id="char_no" placeholder="" />
                                    </div>
                                </div>
                                <p align="center"><button class="btn btn-primary nextBtn custom-btn" type="button">Next</button></p>
                            </div>

                            <div class="panel panel-primary setup-content" id="step-3">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">Finish</h3>
                                    <h4 class="text-center"><small>Finish Up</small></h4>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label" for="fans_action">What will you like your fans to do?</label>
                                        <select id="fans_action" name="fans_action" class="form-control">
                                            <option value="none">None</option>
                                            <option value="download">Download</option>
                                            <option value="website-visit">Website / Blog Visit</option>
                                            <option value="social_media_follow">Social Media Follow</option>
                                        </select>
                                    </div>
                                    <div id="download" class="form-group fans_action_selected" style="display:none;">
                                        <label for="download_link" class="float-left">Download Link</label>
                                        <input type="text" class="form-control" name="download_link" id="download_link" />
                                    </div>
                                    <div id="website-visit" class="form-group fans_action_selected" style="display:none;">
                                        <label for="website_url" class="float-left">Website URL</label>
                                        <input type="text" class="form-control" name="website_url" id="website_url" />
                                    </div>
                                    <div id="social_media_follow" class="fans_action_selected" style="display:none;">
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input type="text" class="form-control" name="twitter" id="twitter" />
                                        </div>
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input type="text" class="form-control" name="instagram" id="instagram" />
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input type="text" class="form-control" name="facebook" id="facebook" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="reward_type">Choose Reward Type</label>
                                        <select id="reward_type" name="reward_type" class="form-control">
                                            <option value="">Please select</option>
                                            <option value="cash">Cash</option>
                                            <option value="gift_product">Gift / Product</option>
                                            <option value="ticket">Ticket</option>
                                        </select>
                                    </div>
                                    <div id="cash" class="reward_type_selected" style="display:none;">
                                        <div class="form-group">
                                            <label for="reward_people_count">How many people to reward?</label>
                                            <input type="number" class="form-control" name="count_reward_winners" id="reward_people_count">
                                        </div>
                                        <div class="form-group">
                                            <label for="reward_amount">How much to reward?</label>
                                            <input type="number" class="form-control" name="reward_amount" id="reward_amount">
                                        </div>
                                    </div>
                                    <div id="gift_product" class="reward_type_selected" style="display:none;">
                                        <div class="form-group">
                                            <label for="gift_people_count">How many people to give gift?</label>
                                            <input type="number" class="form-control" name="gift_people_count" id="gift_people_count">
                                        </div>
                                        <div class="form-group">
                                            <label for="gift_name">Name of Gift / Product</label>
                                            <input type="text" class="form-control" name="gift_product_name" id="gift_name">
                                        </div>
                                    </div>
                                    <div id="ticket" class="reward_type_selected" style="display:none;">
                                        <div class="form-group">
                                            <label for="ticket_people_count">How many people to Give Ticket?</label>
                                            <input type="number" class="form-control" name="ticket_people_count" id="ticket_people_count">
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket_type">Type of ticket</label>
                                            <input type="text" class="form-control" name="ticket_type" id="ticket_type" placeholder="E.g Movie Ticket, Event Ticket">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label" for="reward_type">Choose Game Type</label>
                                        <select id="paid_type" name="game_type" class="form-control">
                                            <option value="">Please select</option>
                                            <option value="free">Free</option>
                                            <option value="paid">Paid</option>
                                        </select>
                                    </div>
                                    <div id="paid" class="paid_type_selected" style="display: none">
                                        <div class="form-group">
                                            <label for="paid_game_amount">Enter Amount</label>
                                            <input type="number" id="paid_game_amount" class="form-control">
                                        </div>
                                    </div>
                                    <p align="center">
                                        <button class="btn btn-primary nextBtn custom-btn" type="submit">
                                            Submit
                                            <i class="fa fa-spinner fa-spin" style="display: none;" id="create-spin"></i>
                                        </button>
                                    </p>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- =====================================
    ==== Auto Show Game Type -->
    <script>
        $(function () {
            $('#game_type').change(function () {
                $('.game_type_selected').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>

    <!-- =====================================
    ==== Fans Action -->
    <script>
        $(function () {
            $('#fans_action').change(function () {
                $('.fans_action_selected').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>

    <script>
        $(function () {
            $('#reward_type').change(function () {
                $('.reward_type_selected').hide();
                $('#' + $(this).val()).show();
            })
        })
    </script>

    <!-- =====================================
    ==== Reward Type -->
    <script>
        $(function () {
            $('#paid_type').change(function () {
                $('.paid_type_selected').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>

    <!-- =====================================
    ==== Game Type -->
    <script>
        $(function () {
            $('#game_type').change(function () {
                $('.game_type_selected').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>

    <script>
        $(document).ready(function () {

            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        });
    </script>
    <script>
        // Wait until the window finishes loaded before executing any script
        window.onload = function () {

            // Initialize the activityNumber
            var activityNumber = 2;

            // Select the add_activity button
            var addButton = document.getElementById("add");

            // Select the table element
            var tracklistTable = document.getElementById("q");

            // Attach handler to the button click event
            addButton.onclick = function () {

                // Create New Table Row Element
                var newNode = document.createElement('div');

                // Set the Inner HTML of that element
                newNode.innerHTML = '<hr class="bg-info" /> <div class="form-group"> <h4 class="text-center p-2 text-info" style="background-color: #1976d2; color: #fff; margin-bottom: 2%;"> <small style="color: #fff;"> Question ' +
                    activityNumber + '</small> </h4> <textarea type="text" class="form-control" name="question[]"></textarea></div> <div class="form-group"> <h4 class="text-center"> <small>Option A</small> </h4> <input type="text" class="form-control" name="option_a[]"> </div> <div class="form-group"> <h4 class="text-center"> <small>Option B</small> </h4> <input type="text" class="form-control" name="option_b[]"> </div> <div class="form-group"> <h4 class="text-center"> <small >Option C</small></h4> <input type="text" class="form-control" name="option_c[]"> </div> <div class="form-group">  <h4 class="text-center"> <small>Option D</small> </h4> <input type="text" class="form-control" name="option_d[]"> </div> <div class="form-group"> <h4 class="text-center"> <small>Correct Option (Answer) </small> </h4>  <select name="answer[]" class="form-control"><option value="1">Option A</option> <option value="2">Option B</option> <option value="3">Option C</option> <option value="4">Option D</option><select>';

                // Append the element to the tracklistTable
                tracklistTable.appendChild(newNode);


                // Increment the activityNumber
                activityNumber += 1;
            }


        }



        function change(type) {
            var selectBox = type;
            var selected = selectBox.options[selectBox.selectedIndex].value;
            var textarea = document.getElementById("url");

            if(selected === '2'){
                textarea.style.display = "block";
            }
            else{
                textarea.style.display = "none";
            }
        }
    </script>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/create_essay.js') }}"></script>
@endsection
