@extends('Dashboard.template')

@section('section-main')
    <div class="row" style="padding: 3% 3% 1%;background-color: #fff; border-bottom: 1px solid #eee;">
        <div class="col-lg-6">
            <h5>Edit Game</h5>
        </div>
        <div class="col-lg-6">
            <p class="text-right">{{ ucwords($userGame->quiz_title)  }}</p>
        </div>
    </div>
    <div class="row" style="padding: 3% 3% 1%;background-color: #fff;">
        <div class="col-lg-12">
            <form id="update_form">

                <div class="form-group">
                    <label for="quiz_title">Game Description</label>
                    <input type="text" value="{{ $userGame->essay_title }}" name="essay_title" class="form-control input-default" id="quiz_title">
                </div>
                <div class="form-group">
                    <label class="control-label" for="essay_description">Essay Description</label>
                    <textarea class="form-control" id="essay_description" rows="5" cols="10" maxlength="1800" name="essay_description" placeholder="Give a description of what you want your audience to write about.">{{ strip_tags($userGame->essay_description) }}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" onclick="updateEssayGame({{ $userGame->id }})" style="width: 100%; color: #fff;">
                        UPDATE GAME
                        <i class="fa fa-spinner fa-spin" id="spinner" style="display: none;"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/edit_game.js') }}"></script>
@endsection