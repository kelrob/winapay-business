<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li>
                <a href="{{ url('dashboard') }}" aria-expanded="false">
                    <i class="icon-speedometer menu-icon"></i><span class="nav-text">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/create-campaign') }}" aria-expanded="false">
                    <i class="icon-plus menu-icon"></i><span class="nav-text">Create Campaign</span>
                </a>
            </li>
            <li>
                <a href="{{ url('essay-response') }}" aria-expanded="false">
                    <i class="icon-envelope align-middle mr-2"></i><span class="nav-text">Essay Response</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/statistics') }}" aria-expanded="false">
                    <i class="icon-graph menu-icon"></i><span class="nav-text">Statistics</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/profile') }}" aria-expanded="false">
                    <i class="icon-user menu-icon"></i><span class="nav-text">Profile</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/fund-wallet') }}" aria-expanded="false">
                    <i class="icon-wallet menu-icon"></i><span class="nav-text">Fund Wallet</span>
                </a>
            </li>
            <li>
                <a href="{{ url('how-it-works') }}" aria-expanded="false">
                    <i class="fa fa-video-camera menu-icon"></i><span class="nav-text">How it works</span>
                </a>
            </li>
            {{--<li>--}}
                {{--<a href="{{ url('contact-us') }}" aria-expanded="false">--}}
                    {{--<i class="icon-envelope-letter menu-icon"></i><span class="nav-text">Contact Us</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li>
                <a href="{{ url('/sign-out') }}" aria-expanded="false">
                    <i class="icon-logout menu-icon"></i><span class="nav-text">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>
