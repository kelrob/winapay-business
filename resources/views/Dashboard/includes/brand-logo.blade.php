<div class="brand-logo" style="background-color: #fff;">
    <a href="#">
        <b class="logo-abbr"><h3>W</h3></b>
        <span class="logo-compact"><img src="https://winapay.com/img/logo-dark.png" alt=""></span>
        <span class="brand-title">
            <img src="https://winapay.com/img/logo-dark.png" alt="" id="brand-logo">
        </span>
    </a>
</div>