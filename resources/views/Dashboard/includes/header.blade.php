<div class="header" style="-webkit-box-shadow: 1px 1px 10px 1px #eee;
    -moz-box-shadow: 1px 1px 10px 1px #eee;
    box-shadow: 1px 1px 10px 1px #eee;">
    <div class="header-content clearfix">

        <div class="nav-control">
            <div class="hamburger">
                <span class="toggle-icon"><i class="icon-menu"></i></span>
            </div>
        </div>
        <div class="header-left">
            <!--
            <div class="input-group icons">
                <div class="input-group-prepend">
                        <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i
                                    class="mdi mdi-magnify"></i></span>
                </div>
                <input type="search" class="form-control" placeholder="Search Dashboard"
                       aria-label="Search Dashboard">
                <div class="drop-down animated flipInX d-md-none">
                    <form action="#">
                        <input type="text" class="form-control" placeholder="Search">
                    </form>
                </div>
            </div>
            -->
        </div>
        <div class="header-right">
            <ul class="clearfix">
                <li class="icons dropdown d-none d-md-flex">
                    <a href="javascript:void(0)" class="log-user" data-toggle="dropdown">
                        <span style="font-weight: 500;">
                            <a href="{{ url('create-campaign') }}" class="btn btn-primary" style="padding: 2% 4%; color: #fff; border-radius: 20px;">
                                Create Campaign
                            </a>
                        </span>
                        &nbsp; &nbsp;
                        <span style="font-weight: 500;">Hi, {{ $loggedUser->business_name }} </span>
                        <!--<i class="fa fa-angle-down f-s-14" aria-hidden="true"></i>-->
                    </a>
                    <!--
                    <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <li><a href="#">English</a></li>
                                <li><a href="#">Dutch</a></li>
                            </ul>
                        </div>
                    </div>
                    -->
                </li>
                <li class="icons dropdown">
                    <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                        <span class="activity active"></span>
                        @if ($loggedUser->logo == null)
                            <img src="img/blank.png" height="40" width="40" alt="">
                        @else
                            <img src="{{ url('img/logos/' .$loggedUser->logo) }}" height="40" width="40" alt="">
                        @endif
                    </div>
                    <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="{{ url('profile') }}"><i class="icon-user"></i> <span>Profile</span></a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="icon-envelope-open"></i> <span>Notification</span>--}}
{{--                                        <div class="badge gradient-3 badge-pill gradient-1">3</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}

                                <hr class="my-2">
                                <li><a href="{{ url('/sign-out') }}"><i class="icon-key"></i> <span>Logout</span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
