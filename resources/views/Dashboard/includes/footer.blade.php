<div class="footer">
    <div class="copyright">
        <p>Copyright &copy; {{ date('Y') }} Designed & Developed by <a href="https://winapay.com">WINAPAY</a>
        </p>
    </div>
</div>