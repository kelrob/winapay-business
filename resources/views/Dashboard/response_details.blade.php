@extends('Dashboard.template')

@section('section-main')
    <script>
        function updateTextInput(val) {
            document.getElementById('textInput').textContent= val + '%';
        }
    </script>
    <section>
        <div class="row" style="background-color: #fff; padding: 2%;">
            <div class="col-lg-12"><h3>{{ userData($response->win_user_id)->fullname }}</h3></div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="media media-reply">
                            {{--<img class="mr-3 circle-rounded" src="{{ userData($response->win_user_id)->avatar }}" width="50" height="50" alt="Generic placeholder image">--}}
                            <div class="media-body">
                                <div class="d-sm-flex justify-content-between mb-2">
                                    <h5 class="mb-sm-0">{{ userData($response->win_user_id)->fullname }}
                                        <small class="text-muted ml-3">exactly {{ $response->created_at->diffForHumans() }}</small>
                                    </h5>

                                    @if($response->score == 0)
                                        <div class="media-reply__link">
                                            <button
                                                    class="btn btn-info font-weight-bold"
                                                    style="background-color: #1976d2"
                                                    data-toggle="modal" data-target="#exampleModalCenter"
                                            >
                                                Award Score
                                                <i class="fa fa-check" style="color: #fff;"></i>
                                            </button>
                                        </div>
                                    @else
                                        <div class="media-reply__link">
                                            <button
                                                    class="btn btn-info font-weight-bold"
                                                    style="background-color: #41B67E; border-style: none"
                                            >
                                                {{ $response->score }} / 100
                                                <i class="fa fa-check" style="color: #fff;"></i>
                                            </button>
                                        </div>
                                    @endif
                                </div>

                                <div id="essay-response">
                                    {!! $response->response !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="exampleModalCenter" style="margin-top: -6%;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Award Score to {{ userData($response->win_user_id)->fullname }}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="award-score-form">
                        <div class="input-group mb-3">
                            <input id="range" type="range" min="0" max="100" onchange="updateTextInput(this.value);" class="form-control" placeholder="Recipient's username">
                            <div class="input-group-append" style="background-color: #1976d2;">
                                <span class="input-group-text" style="background-color: #1976d2; border-radius: 0; color: #fff;" id="textInput">
                                    50%
                                </span>
                            </div>
                        </div>
                        <div class="form-group" align="center">
                            <button type="button" class="btn btn-info" onclick="awardScore({{ $response->win_user_id }}, {{ $response->essay_id }})" id="btn-submit">
                                <span id="btn-text">Submit</span>
                                <span id="submit-loader" style="display: none;"><i class="fa fa-spinner fa-spin"></i></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/response_details.js') }}"></script>
@endsection