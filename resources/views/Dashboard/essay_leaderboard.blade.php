@extends('Dashboard.template')

@section('section-main')
    <section>
        <div class="row" style="background-color: #fff; padding: 2%;">
            <div class="col-lg-12">
                <h5 class="text-dark">
                    {{ $game->essay_title }} Leaderboard
                    <span class="pull-right"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#rewardModal">Reward Winners &nbsp;<i class="fa fa-trophy"></i></button></span>
                </h5>
            </div>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Position</th>
                            <th>Name of user</th>
                            <th>Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($response as $quizResponse)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a href="{{ url('essay-response/details/' . $quizResponse->id) }}">
                                        {{ userData($quizResponse->win_user_id)->fullname }}
                                    </a>
                                </td>
                                <td>
                                    {{ $quizResponse->score }} / 100
                                    &nbsp;
                                    @if($quizResponse->winner == 1)
                                        <span class="badge-success p-1 text-white" style="border-radius: 4px;">winner</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="rewardModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="text-center mt-3">Are you sure you want to reward users now?</h4>
                    <p class="text-center mt-3">
                        <button class="btn btn-sm btn-success btn-rounded" id="reward_btn" onclick="rewardUser({{ $game->id }})" style="background-color: #0AA639; border-style: none; color: #fff; width: 10%;">Yes</button>
                        &nbsp; &nbsp;
                        <button class="btn btn-sm btn-danger btn-rounded" data-dismiss="modal" style="color: #fff; width: 10%;">No</button>
                    </p>
                    <p class="text-center" id="reward-text" style="display: none;"><small><b>Rewarding user please wait <i class="fa fa-spinner fa-spin"></i></b></small></p>
                    <div id="reward-response"></div>
                </div>
            </div>

        </div>
    </div>

    <!-- Javascript -->
    <script src="{{ url('template/js/essay_leaderboard.js') }}"></script>
@endsection