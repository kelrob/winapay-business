@extends('Dashboard.template')

@section('section-main')

    <div class="row">
        <div class="col-md-12" style="">
            <h3 class="text-center">Create a Campaign for your fans</h3>
        </div>
    </div>

    <div class="row" style="margin-top: 2%;">
        <div class="col">
            <div class="card">
                <div class="card-body" align="center">
                    <img src="{{  url('img/quiz.jpg') }}" style="max-width: 100px;" alt="quiz icon" />

                    <p style="margin-top: 3%;">
                        <a href="{{ url('create-campaign/quiz') }}" class="btn btn-primary" style="background-color: #1976D2; border-style: none;">
                            Create a Quiz Campaign
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body" align="center">
                    <img src="{{ url('img/essay.png') }}" style="max-width: 100px;" alt="quiz icon" />

                    <p style="margin-top: 3%;">
                        <a href="{{ url('create-campaign/essay') }}" class="btn btn-primary" style="background-color: #1976D2; border-style: none;">
                            Create an Essay Campaign
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection