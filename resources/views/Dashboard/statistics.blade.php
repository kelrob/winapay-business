@extends('Dashboard.template')

@section('section-main')
    <div class="row">
        <div class="col-md-12" style="background-color: #fff; padding-top: 2%;">
            <div class="panel panel-default">
                <div class="panel-heading">Chart</div>
                <div class="panel-body">
                    {!! $chart->html() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 4%;">
        <div class="col-lg-8">
            <div class="card" style="border-radius: 0;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-9" style="background-color: #fff; padding-top: 2%;">

                            <div class="panel-body">
                                {!! $pie->html() !!}
                            </div>
                        </div>
                        <div class="col-lg-3" style="background-color: #fff; padding-top: 2%;">
                            <h6><u>Age Demography</u></h6>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><small>Range (yrs)</small></th>
                                        <th><small>Total</small></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><small>18 - 25</small></td>
                                    <td><small>12k</small></td>
                                </tr>
                                <tr>
                                    <td><small>18 - 25</small></td>
                                    <td><small>12k</small></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card" style="border-radius: 0;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12" style="background-color: #fff; padding-top: 2%;" align="center">
                            <div class="panel-heading">Total Paid Subscribers</div>
                            <div class="panel-body"><h2>{{ approxFigure(paidSubscribers($loggedUser->id)->count) }}</h2></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
    {!! $pie->script() !!}
@endsection