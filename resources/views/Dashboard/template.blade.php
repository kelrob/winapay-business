<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Winapay Business Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/favicon.ico') }}">

    <!-- Pignose Calender -->
    <link href="{{ url('./template/plugins/pg-calendar/css/pignose.calendar.min.css') }}" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="{{ url('./template/plugins/chartist/css/chartist.min.css') }}">
    <link rel="stylesheet"
          href="{{ url('./template/plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css') }}">
    <!-- Custom Stylesheet -->
    <link href="{{ url('template/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('template/css/custom.css') }}" rel="stylesheet">
    <link href="{{ url('./template/plugins/toastr/css/toastr.min.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
        Nav header start
    ***********************************-->
    <div class="nav-header">
        @include('Dashboard.includes.brand-logo')
    </div>
    <!--**********************************
        Nav header end
    ***********************************-->

    <!--**********************************
        Header start
    ***********************************-->
    @include('Dashboard.includes.header')
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        Sidebar start
    ***********************************-->
    @include('Dashboard.includes.sidebar')
    <!--**********************************
        Sidebar end
    ***********************************-->

    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="container-fluid mt-3">
            @yield('section-main')
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->
    @include('Dashboard.includes.footer')
    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="{{ url('template/plugins/common/common.min.js') }}"></script>
<script src="{{ url('template/js/custom.min.js') }}"></script>
<script src="{{ url('template/js/settings.js') }}"></script>
<script src="{{ url('template/js/gleek.js') }}"></script>
<script src="{{ url('template/js/styleSwitcher.js') }}"></script>

<!-- Chartjs -->
<script src="{{ url('./template/plugins/chart.js/Chart.bundle.min.js') }}"></script>
<!-- Circle progress -->
<script src="{{ url('./template/plugins/circle-progress/circle-progress.min.js') }}"></script>
<!-- Datamap -->
<script src="{{ url('./template/plugins/d3v3/index.js') }}"></script>
<script src="{{ url('./template/plugins/topojson/topojson.min.js') }}"></script>
<script src="{{ url('./template/plugins/datamaps/datamaps.world.min.js') }}"></script>
<!-- Morrisjs -->
<script src="{{ url('./template/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ url('./template/plugins/morris/morris.min.js') }}"></script>
<!-- Pignose Calender -->
<script src="{{ url('./template/plugins/moment/moment.min.js') }}"></script>
<script src="{{ url('./template/plugins/pg-calendar/js/pignose.calendar.min.js') }}"></script>
<!-- ChartistJS -->
<script src="{{ url('./template/plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ url('./template/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js') }}"></script>


<script src="{{ url('./template/js/dashboard/dashboard-1.js') }}"></script>

<!-- Toastr -->
<script src="{{ url('./template/plugins/toastr/js/toastr.min.js') }}"></script>
<script src="{{ url('./template/plugins/toastr/js/toastr.init.js') }}"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5eac867d203e206707f8bbe0/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>