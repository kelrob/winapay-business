@extends('Dashboard.template')

@section('section-main')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Chart</div>

                <div class="panel-body">
                    {!! $chart->html() !!}
                </div>
            </div>
        </div>
    </div>

    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
@endsection