@extends('Dashboard.template')
@section('section-main')

    <div class="row">
        <div class="col-lg-7">
            <div class="card" style="border-radius: 0;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>How it works</h4>
                        </div>
                        <div class="col-lg-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/EaX346NbHTM"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="card" style="border-radius: 0;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 mb-4">
                            <h4>Contact Us</h4>
                        </div>
                        <div class="col-lg-12">
                            <h5><b><span class="fa fa-phone"></span></b> PHONE: +234 705 984 5923</h5>
                            <h5><b><span class="fa fa-envelope"></span></b> Email: <a href="mailto:info@winapay.com">info@winapay.com</a></h5>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
