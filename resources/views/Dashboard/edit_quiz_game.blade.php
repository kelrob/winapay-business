@extends('Dashboard.template')

@section('section-main')
    <div class="row" style="padding: 3% 3% 1%;background-color: #fff; border-bottom: 1px solid #eee;">
        <div class="col-lg-6">
            <h5>Edit Game</h5>
        </div>
        <div class="col-lg-6">
            <p class="text-right">{{ ucwords($userGame->quiz_title)  }}</p>
        </div>
    </div>
    <div class="row" style="padding: 3% 3% 1%;background-color: #fff;">
        <div class="col-lg-12">
            <form id="update_form">
                <div class="form-group">
                    <label for="quiz_title">Game Title</label>
                    <input type="text" value="{{ $userGame->quiz_title }}" name="quiz_title" class="form-control input-default" id="quiz_title">
                </div>

                @if ($userGame->game_type == 'post')
                    <div class="form-group">
                        <label for="post_writeup">Post Write Up</label>
                        <textarea class="form-control input-default" rows="4" cols="10" maxlength="1800" name="post_writeup" id="post_writeup">{{ strip_tags($userGame->post_writeup) }}</textarea>
                    </div>
                @elseif ($userGame->game_type == 'video')
                    <div class="form-group">
                        <label for="embedded_url">Embedded URL</label>
                        <input type="text" name="embedded_url" class="form-control input-default" value="{{ $userGame->embedded_url }}" id="embedded_url">
                    </div>
                    <div class="form-group">
                        <label for="video_desc">Short Video Description (Max 400 Chars)</label>
                        <textarea id="video_desc" class="form-control" maxlength="400" name="video_desc">{{ strip_tags($userGame->video_desc)  }}</textarea>
                    </div>
                @endif

                @foreach($data['game_question'] as $item => $question)
                    <div class="form-group">
                        <label for="question" style="background-color: #2e6da4; color: #fff; padding: 0.5%; border-radius: 2px;">Question {{ $loop->iteration }}</label>
                        <textarea id="question" class="form-control" maxlength="400" name="question[]">{{ $question }}</textarea>
                    </div>
                    <div class="form-group">
                        <label><b>Option A</b></label>
                        <input name="option_a[]" maxlength="200" value="{{ $data['option_a'][$loop->index] }}" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label><b>Option B</b></label>
                        <input name="option_b[]" maxlength="200" value="{{ $data['option_b'][$loop->index] }}" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label><b>Option C</b></label>
                        <input name="option_c[]" maxlength="200" value="{{ $data['option_c'][$loop->index] }}" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label><b>Option D</b></label>
                        <input name="option_d[]" maxlength="200" value="{{ $data['option_d'][$loop->index] }}" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="correct_answer"><b>Correct Answer</b></label>
                        <select name="answer[]" class="form-control">
                            @if($data['answer'][$loop->index] == 1)
                                <option value="1"> Option A</option>
                            @endif

                            @if($data['answer'][$loop->index] == 2)
                                <option value="2"> Option B</option>
                            @endif

                            @if($data['answer'][$loop->index] == 3)
                                <option value="3"> Option C</option>
                            @endif

                            @if($data['answer'][$loop->index] == 4)
                                <option value="4"> Option B</option>
                            @endif

                            <option value="1"> Option A</option>
                            <option value="2"> Option B</option>
                            <option value="3"> Option C</option>
                            <option value="4"> Option D</option>
                        </select>
                    </div>
                    <hr />
                @endforeach
                <div class="form-group">
                    <label><b>Time per Question</b></label>
                    <select name="time" id="timer" class="form-control">
                        @if($userGame->time == 5)
                            <option value="5">5 sec</option>
                        @endif
                        @if($userGame->time == 10)
                            <option value="10">10 sec</option>
                        @endif
                        @if($userGame->time == 15)
                            <option value="15">15 sec</option>
                        @endif
                        @if($userGame->time == 20)
                            <option value="20">20 sec</option>
                        @endif
                        <option value="5">5 sec</option>
                        <option value="10">10 sec</option>
                        <option value="15">15 sec</option>
                        <option value="20">20 sec</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" onclick="updateGame({{ $userGame->id }})" style="width: 100%; color: #fff;">
                        UPDATE GAME
                        <i class="fa fa-spinner fa-spin" id="spinner" style="display: none;"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/edit_game.js') }}"></script>
@endsection