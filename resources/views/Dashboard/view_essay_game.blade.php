@extends('Dashboard.template')

@section('section-main')

    <div class="row" style="padding: 3%; background-color: #fff;">
        <div class="col-md-7">
            <h4>{{ ucwords($userGame->essay_title)  }}</h4>
            <p>
                <img src="{{ url('img/campaigns/'. $userGame->cover_photo) }}" class="img-responsive">
            </p>
            <p>
                {!! $userGame->essay_description !!}
            </p>

        </div>

        <div class="col-md-5" style="border-left: 1px dotted #eee;">
            <div style="border-bottom: 1px solid #eee; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                <p class="text-center">
                    <a href="{{ url('edit-game/essay/' . $userGame->id . '/' . $userGame->game_permalink) }}" class="btn btn-sm btn-primary">Edit game</a>
                    &nbsp;
                    @if($userGame->game_closed == 0)
                        <span id="close-open-btn">
                        <a onclick="closeGame({{ $userGame->id }})" class="btn btn-sm btn-danger" id="close-game-btn">
                            Close game &nbsp;
                            <i class="fa fa-spinner fa-spin" id="close-game-loader" style="display: none;"></i>
                        </a>
                    </span>
                    @elseif($userGame->game_closed == 1)
                        <span id="close-open-btn">
                        <a onclick="openGame({{ $userGame->id }})" class="btn btn-sm btn-success" id="open-game-btn">
                            Open game &nbsp;
                            <i class="fa fa-spinner fa-spin" id="open-game-loader" style="display: none;"></i>
                        </a>
                    </span>
                    @endif
                    &nbsp;
                    <a href="#" data-toggle="modal" data-target="#delete-game" class="btn btn-sm btn-outline-danger">Delete game</a>
                </p>
                <p class="text-center">
                    Game Status <br />
                    <span id="game-status">
                        @if($userGame->game_closed == 1)
                            <b style="color: #f44336">Closed</b>
                        @elseif($userGame->game_closed == 0)
                            <b style="color: #11772d;">Opened</b>
                        @endif
                    </span>
                    <br />
                </p>
            </div>
        </div>
    </div>

    <div id="delete-game" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" align="center">
                <div class="modal-body">

                    <h4 class="text-center" align="center">
                        Would you like to delete this Game?
                    </h4>
                    <hr />
                    <p class="text-center">
                        <button class="btn btn-sm btn-danger btn-rounded" id="yes" onclick="deleteGame({{ $userGame->id }})" style="width: 15%;">
                            Yes &nbsp;<i id="del-loader" class="fa fa-spinner fa-spin" style="display: none;"></i>
                        </button>
                        &nbsp;
                        <a href="#" class="btn btn-sm btn-primary btn-rounded" data-dismiss="modal" style="width: 15%;">No</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/view_game.js') }}"></script>
@endsection