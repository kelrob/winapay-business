@extends('Dashboard.template')

@section('section-main')
    <div class="row" style="background-color: #fff; padding: 2%;">
        <div class="col-md-7">
            <h4>Edit Profile</h4>
            <div style="margin-top: 2%; border-top: 1px solid #eee; padding-top: 3%;">
                <form id="update-profile-form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="brand-name">Brand Name</label>
                        <input type="text" class="form-control" name="business_name" id="brand-name" disabled="disabled" value="{{ $loggedUser->business_name }}">
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="brand-website">Website</label>
                            <input type="text" class="form-control" name="website" id="brand-website" value="{{ $loggedUser->website }}">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="brand-phone">Contact Phone</label>
                            <input type="text" class="form-control" name="phone" id="brand-phone" value="{{ $loggedUser->phone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="brand-address">Business Address</label>
                        <input type="text" class="form-control" name="address" id="brand-address" value="{{ $loggedUser->address }}">
                    </div>
                    <div class="form-group">
                        <label for="brand-color">Brand Color</label>
                        <select name="brand_color" id="brand-color" class="form-control">
                            <option value="{{ $loggedUser->brand_color }}">{{ $loggedUser->brand_color }}</option>
                            <option value="blue">Blue</option>
                            <option value="green">Green</option>
                            <option value="yellow">Yellow</option>
                            <option value="red">Red</option>
                            <option value="orange">Orange</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="brand-facebook">Facebook page Url</label>
                            <input type="text" class="form-control" name="facebook_page" id="brand-facebook" value="{{ $loggedUser->facebook_page }}">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="brand-twitter">Twitter page Url</label>
                            <input type="text" class="form-control" name="twitter_page" id="brand-twitter" value="{{ $loggedUser->twitter_page }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="brand-instagram">Instagram page Url</label>
                            <input type="text" class="form-control" name="instagram_page" id="brand-instagram" value="{{ $loggedUser->instagram_page }}">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="brand-whatsapp">Chat with us URL</label>
                            <input type="text" class="form-control" name="whatsapp_no" placeholder="" id="brand-whatsapp" value="{{ $loggedUser->whatsapp_no }}">
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit" id="update-btn" style="background-color: #2e6da4; width: 100%;">
                        UPDATE PROFILE
                        <i class="fa fa-spinner fa-spin" style="display: none;" id="update-spin"></i>
                    </button>
                </form>
            </div>
        </div>
        <div class="col-md-5" style="border-left: 1px dotted #eee;">
            <h4 class="text-center">Brand Logo</h4>
            <p align="center" style="border-bottom: 1px solid #eee; padding-bottom: 4%;">
                @if ($loggedUser->logo == null)
                    <img
                            class="img-circle"
                            src="img/blank.png"
                            height="120" width="120"
                            alt=""
                    >
                @else
                    <img
                            class="img-circle"
                            src="{{ url('img/logos/' .$loggedUser->logo) }}"
                            height="120" width="120"
                            alt=""
                    >
                @endif
                 <br />
                <small><a href="#" data-toggle="modal" data-target="#logo-modal" style="color: #7571f9;" class="active">Change Logo</a></small>
            </p>

            <h5>Basic Profile</h5>
            <p style="line-height: 25px;">
                <b>{{ $loggedUser->business_name }}</b>
                <br />
                <i class="fa fa-globe"></i> &nbsp; {{ $loggedUser->website }}
                <br />
                <i class="fa fa-map-marker"></i> &nbsp; {{ $loggedUser->address }}
                <br />
                <i class="fa fa-phone"></i> &nbsp; {{ $loggedUser->phone }}
                {{--<br />--}}
                {{--<i class="fa fa-circle" style="color: #2e6da4"></i> &nbsp; Brand Color--}}
            </p>

            <h5 style="margin-top: 1%;">Social Media</h5>
            <i class="fa fa-facebook-square fa-2x" style="color: #3C5A99;"></i> &nbsp; {{ $loggedUser->facebook_page }}
            <br /> <br />
            <i class="fa fa-twitter-square fa-2x" style="color: #00A7E7;"></i> &nbsp; {{ $loggedUser->twitter_page }}
            <br /> <br />
            <i class="fa fa-instagram fa-2x" style="color: #3f729b;"></i> &nbsp; {{ $loggedUser->instagram_page }}
            <br /> <br />
            <i class="fa fa-commenting fa-2x" style="color: #3f729b;"></i> &nbsp; {{ $loggedUser->whatsapp_no }}
            <br /> <br />

            <hr />
            <h4 style="margin-top: 1%;">Subscription Mode:
                @if($loggedUser->brand_subscription_mode->status == 0)
                    <span class="text-danger">OFF</span>
                    <p class="text-center mt-2">
                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#subscriptionModal" style="width: 100%;">Turn On</button>
                    </p>
                @elseif($loggedUser->brand_subscription_mode->status == 1)
                    <span class="text-success">ON</span>
                    <span class="pull-right"><small>Amount: &#8358;{{ $loggedUser->brand_subscription_mode->amount }}</small></span>
                    <p class="text-center mt-2">
                        <button class="btn btn-danger btn-sm" onclick="offSubscription()" id="btn-off" style="width: 100%;">
                            <i class="fa fa-spinner fa-spin" style="display: none;" id="btn-off-spin"></i>
                            Turn Off
                        </button>
                    </p>
                @endif
            </h4>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="logo-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Change Logo</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="change-logo-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" name="logo" class="form-control" />
                        </div>
                        <div class="form-group" align="center">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-spinner fa-spin" style="display: none;" id="logo-spin"></i>
                                Change Logo
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!-- Subscription Modal -->
    <div class="modal fade" id="subscriptionModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Turn On Subscription</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p class="text-center font-weight-bold">This Subscription will last a Fan/customer for one Month</p>
                    <form id="subscription-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="amount">Cost Amount</label>
                            <input type="number" min="100" class="form-control" name="amount" id="amount" required>
                        </div>
                        <div class="form-group" align="center">
                            <button type="submit" id="update-s-btn" class="btn btn-primary btn-sm">
                                <i class="fa fa-spinner fa-spin" style="display: none;" id="update-s-spin"></i>
                                Turn On
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/profile.js') }}"></script>
@endsection