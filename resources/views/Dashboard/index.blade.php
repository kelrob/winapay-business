@extends('Dashboard.template')

@section('section-main')
    <section>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card gradient-4">
                    <div class="card-body">
                        <h3 class="card-title text-white">Wallet</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white">&#8358;{{ approxFigure($loggedUser->wallet) }}</h2>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="card gradient-1">
                    <div class="card-body">
                        <h3 class="card-title text-white">Campaigns Created</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white">{{ approxFigure($totGames) }}</h2>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="fa fa-dashcube"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="card gradient-3">
                    <div class="card-body">
                        <h3 class="card-title text-white">Fans</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white">{{ approxFigure($loggedUser->fans->count()) }}</h2>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="card gradient-2">
                    <div class="card-body">
                        <h3 class="card-title text-white">{{ $loggedUser->subscription->plan }} Plan</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white">{{ $loggedUser->subscription->days_left }}</h2>
                            <p class="text-white mb-0">Days Left</p>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="fa fa-smile-o"></i></span>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-lg-12">
                <h5>Campaigns</h5>
            </div>
            @if ($totGames > 0)
                @foreach($userGames as $game)
                    @php
                        #Featured Image
                        $featuredImage = $game->cover_photo;
                        $monthYear = $game->month_year;
                        $imageName = explode("/", $featuredImage);
                        array_shift($imageName);
                        $resizedImage = implode("/", $imageName);
                        $resizedImage = $monthYear . '/210x118/' . $resizedImage;

                    @endphp
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="card-body" id="card-body">
                                <div>
                                    @if ($game->quiz_title == '-')
                                        <a href="{{ url('my-game/essay/' . $game->id . '/' . $game->game_permalink) }}">
                                            <img
                                                    src="{{ url('img/campaigns/' . $resizedImage) }}"
                                                    class="img-responsive"
                                                    alt=""
                                                    style="border-radius: 0.25rem;"
                                            />
                                        </a>

                                        <a href="#">
                                            <h5 class="mt-3 mb-1 text-center">

                                                @if ($game->quiz_title == '-')
                                                    <a href="{{ url('my-game/essay/' . $game->id . '/' . $game->game_permalink) }}">
                                                        {{ ucwords($game->essay_title) }}
                                                    </a>
                                                @else
                                                    <a href="{{ url('my-game/quiz/' . $game->id . '/' . $game->game_permalink) }}">
                                                        {{ ucwords($game->quiz_title) }}
                                                    </a>
                                                @endif
                                            </h5>
                                        </a>
                                        <p class="text-center p-1 m-0 font-weight-bold"><small style="font-weight: bold;"><i class="fa fa-trophy"></i> {{ $game->essay_response->count() }} played</small></p>
                                        <p class="m-0 pt-0 text-center">
                                            <a href="leaderboard/essay/{{ $game->id }}" class="btn btn-info btn-sm">Leaderboard</a>
                                        </p>
                                    @endif

                                    @if ($game->quiz_title != '-')
                                        <a href="{{ url('my-game/quiz/' . $game->id . '/' . $game->game_permalink) }}">
                                            <img
                                                    src="{{ url('img/campaigns/' . $resizedImage) }}"
                                                    class="img-responsive"
                                                    alt=""
                                                    style="border-radius: 0.25rem;"
                                            />
                                        </a>

                                            <a href="#">
                                                <h5 class="mt-3 mb-1 text-center">

                                                    @if ($game->quiz_title == '-')
                                                        <a href="{{ url('my-game/essay/' . $game->id . '/' . $game->game_permalink) }}">
                                                            {{ ucwords($game->essay_title) }}
                                                        </a>
                                                    @else
                                                        <a href="{{ url('my-game/quiz/' . $game->id . '/' . $game->game_permalink) }}">
                                                            {{ ucwords($game->quiz_title) }}
                                                        </a>
                                                    @endif
                                                </h5>
                                            </a>
                                            <p class="text-center text-black p-1 m-0 font-weight-bold"><b style=""><small style="font-weight: bold;"><i class="fa fa-trophy"></i> {{ $game->quiz_response->count() }} played</small></b></p>
                                            <p class="m-0 text-center">
                                                <a href="leaderboard/quiz/{{ $game->id }}" class="btn btn-info btn-sm">Leaderboard</a>
                                            </p>
                                    @endif


                                <!-- <a href="#" class="btn btn-sm btn-warning">Send Message</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" id="card-body" style="padding-bottom: 1%;">
                            <h3 class="text-center">No Campaign</h3>
                            <p class="text-center">
                                <a href="{{ url('create-campaign') }}" class="btn btn-primary"
                                   style="letter-spacing: 3px;">CREATE CAMPAIGN <i class="fa fa-cloud-upload"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            @endif
        </div>

    </section>
@endsection