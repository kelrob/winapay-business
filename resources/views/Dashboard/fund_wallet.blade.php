
@extends('Dashboard.template')

@section('section-main')
    <div class="row" style="background-color: #fff; padding: 2%;">
        <div class="col-md-6">
            <h4>Fund Wallet online</h4>
            <form method="post" action="{{ route('pay') }}" accept-charset="UTF-8">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <input type="number" class="form-control" name="amount" id="amount" required onkeyup="changeAmount()">
                    <input type="hidden" name="email" value="{{ $loggedUser->email }}">
                    <input type="hidden" name="metadata" value="{{ json_encode($array = ['type' => 'business', 'uid' => $loggedUser->id]) }}" >
                    <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                    <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Fund">
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <h4>Make a bank transfer</h4>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="color: #000;">Bank Name</th>
                            <th style="color: #000;">Account Name</th>
                            <th style="color: #000;">Bank Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>United Bank of Africa (UBA)</td>
                            <td>WINAPAY LIMITED</td>
                            <td>1021979337</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <p class="text-center ">
                <span class="text-danger"><b>Note:</b> If you are making a payment above &#8358;100,000, Kindly make a bank transfer. <br> <br></span>
                <a href="#" data-toggle="modal" data-target="#bankTransferModal" class="btn btn-primary btn-sm">Click here if you just made a bank transfer.</a>
            </p>
        </div>
    </div>

    <div class="row bg-white p-3 mt-3">
        <div class="col-md-12">
            <h4 class="text-center">Request Funds</h4>
        </div>
        <div class="col-lg-12">
            <p align="center">
                <a class="btn btn-primary btn-sm text-white" data-toggle="modal" data-target="#fundRequestModal">Withdraw from wallet</a>
            </p>
        </div>
    </div>

    <!-- Fund Request Modal -->
    <div class="modal fade" id="fundRequestModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Withdrawal Request</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p class="text-center font-weight-bold">This will be processed within 24 hours</p>
                    <form id="withdrawal-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input type="number" min="30000" class="form-control" name="amount" id="amount" required>
                        </div>
                        <div class="form-group" align="center">
                            <button type="submit" id="withdraw-btn" class="btn btn-primary btn-sm">
                                <i class="fa fa-spinner fa-spin" style="display: none;" id="withdraw-spin"></i>
                                Withdraw
                            </button>

                            <p class="text-center" id="withdraw-text" style="display: none;"><small><b>Placing Request please wait <i class="fa fa-spinner fa-spin"></i></b></small></p>
                            <div id="withdraw-response"></div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!-- Bank Transfer Modal -->
    <div class="modal fade" id="bankTransferModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Fill this form after you have made a bank transfer</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="bank-transfer-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="depositor_name">Depositor Name</label>
                            <input type="text" name="depositor_name" class="form-control" id="depositor_name" />
                        </div>
                        <div class="form-group">
                            <label for="amount_paid">Amount Paid</label>
                            <input type="text" name="amount_paid" class="form-control" id="amount_paid" />
                        </div>
                        <div class="form-group">
                            <label for="payment_date">Date of Payment</label>
                            <input type="text" name="payment_date" class="form-control" id="payment_date" />
                        </div>
                        <div class="form-group" align="center">
                            <button type="submit" id="bank-transfer-btn" class="btn btn-primary btn-sm">
                                <i class="fa fa-spinner fa-spin" style="display: none;" id="bank-transfer-spin"></i>
                                Submit
                            </button>

                            <p class="text-center" id="bank-transfer-text" style="display: none;"><small><b>Submitting Information please wait.. <i class="fa fa-spinner fa-spin"></i></b></small></p>
                            <div id="bank-transfer-response"></div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/fund-wallet.js') }}"></script>
@endsection
