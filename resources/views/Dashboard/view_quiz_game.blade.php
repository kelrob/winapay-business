@extends('Dashboard.template')

@section('section-main')

    <div class="row" style="padding: 3%; background-color: #fff;">
        <div class="col-md-7">
            <h4>{{ ucwords($userGame->quiz_title)  }}</h4>
            <p style="line-height: 25px; text-align: justify">
                @if ($userGame->game_type == 'post')
                    <img src="{{ url('img/campaigns/'. $userGame->cover_photo) }}" class="img-responsive">
                    {!! $userGame->post_writeup !!}
                @endif
            </p>
            <div>
                @if($userGame->game_type == 'video')
                    <div class="embed-responsive embed-responsive-16by9">
                        {!! convertToEmbeddedUrl($userGame->embedded_url) !!}
                    </div>
                    <p class="mt-3">
                        {!! $userGame->video_desc !!}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-md-5" style="border-left: 1px dotted #eee;">
            <div style="border-bottom: 1px solid #eee; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                <p class="text-center">
                    <a href="{{ url('edit-game/quiz/' . $userGame->id . '/' . $userGame->game_permalink) }}" class="btn btn-sm btn-primary">Edit game</a>
                    &nbsp;
                    @if($userGame->game_closed == 0)
                        <span id="close-open-btn">
                        <a onclick="closeGame({{ $userGame->id }})" class="btn btn-sm btn-danger" id="close-game-btn">
                            Close game &nbsp;
                            <i class="fa fa-spinner fa-spin" id="close-game-loader" style="display: none;"></i>
                        </a>
                    </span>
                    @elseif($userGame->game_closed == 1)
                        <span id="close-open-btn">
                        <a onclick="openGame({{ $userGame->id }})" class="btn btn-sm btn-success" id="open-game-btn">
                            Open game &nbsp;
                            <i class="fa fa-spinner fa-spin" id="open-game-loader" style="display: none;"></i>
                        </a>
                    </span>
                    @endif
                    &nbsp;
                    <a href="#" data-toggle="modal" data-target="#delete-game" class="btn btn-sm btn-outline-danger">Delete game</a>
                </p>
                <p class="text-center">
                    Game Status <br />
                    <span id="game-status">
                        @if($userGame->game_closed == 1)
                                <b style="color: #f44336">Closed</b>
                            @elseif($userGame->game_closed == 0)
                                <b style="color: #11772d;">Opened</b>
                            @endif
                    </span>
                    <br />
                    <small><i class="fa fa-clock-o"></i> Timer: {{ $userGame->time }} secs per question</small>
                </p>
            </div>
            <div>
                @foreach($data['game_question'] as $item => $question)

                    <p style="background-color: #2e6da4; padding: 1%; color: #fff; border-radius: 2px;" class="text-center">
                        Question {{ $loop->iteration }}
                    </p>
                    <p class="text-center" style="border: 1px solid #eee; padding: 2%;">
                        {{ $question }}
                    </p>
                    <p class="text-center">
                        <b>A.</b> {{ $data['option_a'][$loop->index] }}
                        <br />
                        <b>B.</b> {{ $data['option_b'][$loop->index] }}
                        <br />
                        <b>C.</b> {{ $data['option_c'][$loop->index] }}
                        <br />
                        <b>D.</b> {{ $data['option_d'][$loop->index] }}
                    </p>
                    <p class="text-center" style="color: #11772d">
                        <b>
                            Answer:
                            @if($data['answer'][$loop->index] == 1)
                                Option A
                            @elseif($data['answer'][$loop->index] == 2)
                                Option B
                            @elseif($data['answer'][$loop->index] == 3)
                                Option C
                            @elseif($data['answer'][$loop->index] == 4)
                                Option D
                            @endif
                        </b>
                    </p>

                @endforeach

            </div>
        </div>
    </div>

    <div id="delete-game" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" align="center">
                <div class="modal-body">

                    <h4 class="text-center" align="center">
                        Would you like to delete this Game?
                    </h4>
                    <hr />
                    <p class="text-center">
                        <button class="btn btn-sm btn-danger btn-rounded" id="yes" onclick="deleteGame({{ $userGame->id }})" style="width: 15%;">
                            Yes &nbsp;<i id="del-loader" class="fa fa-spinner fa-spin" style="display: none;"></i>
                        </button>
                        &nbsp;
                        <a href="#" class="btn btn-sm btn-primary btn-rounded" data-dismiss="modal" style="width: 15%;">No</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Quiz Submission -->
    <script src="{{ url('template/js/view_game.js') }}"></script>
@endsection