@extends('Dashboard.template')
<style>
    a:hover {
        color: #444;
    }
</style>
@section('section-main')
    <section>
        <div class="row" style="background-color: #fff; padding: 2%;">
            <div class="col-lg-12">
                <p style="font-weight: 500;">All Essay Responses</p>
            </div>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table header-border table-hover verticle-middle">
                        <thead>
                        <tr>
                            <th scope="col">Game Title</th>
                            <th scope="col">No of Responses</th>
                            <th scope="col">Action</th>
                            <th scope="col">Status</th>
                            <th scope="col">Time Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($essayGames as $game)
                            <tr>
                                <td>
                                    <a style="color: #2e6da4;" href="{{ url('/my-game/essay/' . $game->id . '/' . $game->game_permalink) }}">
                                        {{ $game->essay_title }}
                                    </a>
                                </td>
                                <td>
                                    <span class="label gradient-1 btn-rounded btn-sm">
                                        {{ $game->essay_response->count() }}
                                    </span>
                                </td>
                                <td>
                                    <a href="{{ url('/essay-response/info/' . $game->id) }}" class="btn btn-primary btn-sm">See Response(s)</a>
                                </td>
                                <td>
                                    @if ($game->game_closed == 1)
                                        <small><span class="text-danger">Closed</span></small>
                                    @else
                                        <small><span class="text-success">Opened</span></small>
                                    @endif
                                </td>
                                <td>
                                    {{ $game->created_at }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection