@extends('Dashboard.template')

@section('section-main')
    <script>
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();

            alert('Link Copied');
        }
    </script>
    <div class="row">
        <div class="col-3"></div>
        <div class="col" style="background-color: #fff; padding: 2%; border-radius: 2px;">
            <h3 style="font-weight: 100; color: #11772d" align="center">
                <span style="font-weight: 100" class="fa fa-check-circle fa-2x"></span>
            </h3>
            <p align="center" style="font-size: 1.2em; margin-top: 0.5%;">Game Created Successfully</p>
{{--            <p style="text-align: center; margin-top: 2%;">Share this Game to fans on the following social media to get them to play</p>--}}
{{--            <hr />--}}
{{--            <p style="margin-top: 4%;" align="center">--}}
{{--                <a data-toggle="modal" data-dismiss="modal" data-target="#challengeModal" style="background-color: #1976d2; color: #fff; padding: 2% 6%; border-radius: 4px;" >--}}
{{--                    Share this Game <i class="fa fa-share"></i>--}}
{{--                </a>--}}
{{--            </p>--}}

            <p id="p1" style="display: none">https://winapay.com/brand-games/{{$gameId}}/{{ $gamePermalink }}</p>
            <p align="center" style="margin-top: 5%;"><a href="#" class="btn btn-outline-primary" onclick="copyToClipboard('#p1')">Copy Link <span class="fa fa-copy"></span></a></p>
        </div>
        <div class="col-3"></div>
    </div>
@endsection
